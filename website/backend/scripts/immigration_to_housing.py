import json
import random
import mysql.connector
from math import radians, cos, sin, asin, sqrt

from pathlib import Path

path = Path(__file__).parent

import os

from dotenv import load_dotenv
load_dotenv()

# Load the jobs dataset, immigration services dataset, and city lat/lon mapping from JSON files
with open(path / "filtered_housing.json", "r") as housing_file:
    housing_data = json.load(housing_file)

with open(path/"immigration.json", 'r') as immigration_file:
    immigration_data = json.load(immigration_file)['features']

# Function to calculate the distance between two points using Haversine formula
def calculate_distance(lat1, lon1, lat2, lon2):
    # Radius of the Earth in miles
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2

    c = 2 * asin(sqrt(a))

    # Radius of the earth in kilometers. Use 3956 for miles
    r = 6371

    # calculate the result
    return c * r * 0.621371

# Function to perform case-insensitive substring search
def case_insensitive_substring(city, text):
    return city.lower() in text.lower()

# Function to get jobs near an immigration service within a specified distance
def get_housing_near_immigration_service(service_lat, service_lon, housing_data, radius):
    nearby_housing = []
    for house in housing_data["results"]:
        house_lat = house['LAT']
        house_lon = house['LON']

        # Calculate the distance between the immigration service and the housing
        distance = calculate_distance(service_lat, service_lon, house_lat, house_lon)

        if distance <= radius:  # Check if the housing is within the specified distance
            nearby_housing.append(house['PROPERTY_ID'])  # Append housing ID to the list

    return nearby_housing

conn = mysql.connector.connect(
    host=os.environ.get("sql_host"),
    user=os.environ.get("sql_username"),
    password=os.environ.get("sql_password"),
    database=os.environ.get("sql_database")
)
cursor = conn.cursor()

# Iterate through each immigration service in the dataset
for service in immigration_data:
    service_lat = service['geometry']['y']
    service_lon = service['geometry']['x']
    service_id = service['attributes']['OBJECTID']

    # Find jobs within a specified distance of the immigration service
    nearby_housing = get_housing_near_immigration_service(service_lat, service_lon, housing_data, 20)

    if len(nearby_housing) < 3:
        nearby_housing = get_housing_near_immigration_service(service_lat, service_lon, housing_data, 49)

    if len(nearby_housing) > 5:
        nearby_housing = random.sample(nearby_housing, 5)

    if len(nearby_housing) > 0:
        # Insert immigration service ID and housing IDs into the MySQL table
        for housing_id in nearby_housing:
            cursor.execute('INSERT INTO immigration_to_house (immigration_id, house_id) VALUES (%s, %s)', (service_id, housing_id))

    # Commit changes to the MySQL database
    conn.commit()

    print(f"Immigration Service ID: {service_id}")
    for housing_id in nearby_housing:
        print("\tHousing ID:", housing_id)

# Close the MySQL database connection
conn.close()