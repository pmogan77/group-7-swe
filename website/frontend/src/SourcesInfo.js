import egis from "./media/egis-logo.jpg";
import hud from "./media/hud-logo.jpg";
import muse from "./media/themuse-logo.jpg";

const sources = [
  {
    name: "Internal Services Dept. Enterprise - GIS Section API",
    image: egis,
  },

  {
    name: "U.S. Dept. of Housing and Urban Development API",
    image: hud,
  },

  {
    name: "The Muse API",
    image: muse,
  },
];

export { sources };
