import { Link } from 'react-router-dom';
import Button from "react-bootstrap/Button";

const HomeCard = ({ title, text, link, image }) => (
    <div className={'card'}>
        <img src={image} className="card-img-top" alt="" />
        <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <p className="card-text">{text}</p>
            <div>
                <Link to={link}>
                    <Button variant="primary" className="custom-button">Learn More</Button>
                </Link>
            </div>
        </div>
    </div>
);

export default HomeCard;
