import requests
import json
from datetime import datetime

api_url_base = 'https://api.la-integration-initiative.me/job'

# Function to fetch data from the API
def fetch_data(page, per_page=1000):
    try:
        response = requests.get(f'{api_url_base}?page={page}&per_page={per_page}')
        data = response.json()
        return data
    except Exception as e:
        print(f'Error fetching data for page {page}: {e}')
        return None

# Function to process the data and group by publication month
def group_by_month(data):
    if not data or not data.get('results'):
        print('Invalid data format or empty results.')
        return None

    month_frequency = {}

    # Process each result
    for item in data['results']:
        publication_date = item.get('publication_date')

        if publication_date:
            # Extract the month from the publication date
            month = datetime.strptime(publication_date, '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m')

            # Initialize or increment the count for the month
            month_frequency[month] = month_frequency.get(month, 0) + 1

    # Sort the dictionary by key (month)
    sorted_month_frequency = dict(sorted(month_frequency.items()))

    return sorted_month_frequency

# Function to write the result to a JSON file
def write_to_json_file(data, filename='job_frequency.json'):
    with open(filename, 'w') as file:
        json.dump(data, file, indent=2)
    print(f'Results written to {filename}')

# Main execution
def main():
    # Make an initial call to get information about total jobs and items per page
    initial_data = fetch_data(1)
    total_jobs = initial_data.get('total_jobs', 0)
    per_page = initial_data.get('per_page', 1000)

    if total_jobs == 0:
        print('Failed to retrieve total jobs information. Exiting.')
        return

    month_frequency = group_by_month(initial_data)

    # Write the final result to a JSON file
    write_to_json_file(month_frequency)

# Run the script
if __name__ == '__main__':
    main()
