import React from "react";
import Container from "react-bootstrap/Container";
import "../css-styles/ModelPage.css";
import RentPieChart from "../components/RentPieChart";
import JobLineGraph from "../components/JobLineGraph";
import ResourceBarGraph from "../components/ResourceBarGraph";


function Visualization() {

    return (
        <Container className="full-page">
            <h1 className="title">Visualizations</h1>
            <Container className="charts-container">
                <h2>Number of Jobs Posted</h2>
                <Container className="job-graph-container">
                    <JobLineGraph />
                </Container>
                <h2>Rent Prices</h2>
                <p> Rounded to the Nearest Hundred</p>
                <Container className="rent-chart-container">
                    <RentPieChart />
                </Container>
                <h2>Resources per Zip Code</h2>
                <Container className="resource-chart-container">
                    <ResourceBarGraph />
                </Container>
            </Container>
        </Container>
    );
}


export default Visualization;