import mysql.connector
import json
import os

from dotenv import load_dotenv
load_dotenv()

def connect_to_database():
    connection = mysql.connector.connect(
        host=os.environ.get("sql_host"),
        user=os.environ.get("sql_username"),
        password=os.environ.get("sql_password"),
        database=os.environ.get("sql_database")
    )
    return connection


def insert_immigration_data(data):
    connection = connect_to_database()
    cursor = connection.cursor()

    for feature in data["features"]:
        attributes = feature["attributes"]

        # Extract data from JSON
        OBJECTID = attributes["OBJECTID"]  # Assuming "OBJECTID" is the JSON field for "id"
        description = attributes["description"]
        name = attributes["Name"]
        hours = attributes["hours"]
        addrln1 = attributes["addrln1"]
        url = attributes["url"]
        latitude = attributes["latitude"]
        longitude = attributes["longitude"]
        community = attributes["city"]
        zip = attributes["zip"]

        # Define the INSERT query with the "id" column
        insert_query = """
            INSERT INTO immigration (id, description, Name, hours, addrln1, url, latitude, longitude, community, zip)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """

        # Execute the INSERT query
        cursor.execute(insert_query, (OBJECTID, description, name, hours, addrln1, url, latitude, longitude, community, zip))

    # Commit the changes and close the connection
    connection.commit()
    cursor.close()
    connection.close()


def read_json_data():
    from pathlib import Path

    path = Path(__file__).parent
    with open(path/"immigration.json", 'r', encoding="utf8") as json_file:
        data = json.load(json_file)
    return data

if __name__ == "__main__":
    data_to_insert = read_json_data()
    insert_immigration_data(data_to_insert)