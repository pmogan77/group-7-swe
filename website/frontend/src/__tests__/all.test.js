import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';

import jobimage from '../media/job-test.png';
import houseimage from '../media/house-test.jpg';
import resourceimage from "../media/immigrant-test.jpg";
import mirys from "../media/miryslist-logo.jpg";
import JobCard from '../components/JobCard.js';
import HomeCard from '../components/HomeCard.js';
import HouseCard from '../components/HouseCard';
import ResourceCard from '../components/ResourceCard';
import immigrantdefenders from "../media/immigrantdefenders-logo.jpg";
import CharityCard from '../components/CharityCard';
import ToolCard from '../components/ToolCard';
import Navbar from '../Navbar';
import MemberCard from '../components/MemberCard';


it('Init HomeCard Job', () => {
    const component = renderer.create(
        <BrowserRouter>
            <HomeCard title="Jobs" text="Find jobs that fit your skills" link="/Jobs" image={jobimage} />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init HomeCard House', () => {
    const component = renderer.create(
        <BrowserRouter>
            <HomeCard title="Housing" text="Discover affordable housing" link="/AffordableHousing" image={houseimage} />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init HomeCard Resource', () => {
    const component = renderer.create(
        <BrowserRouter>
            <HomeCard title="Resources" text="Explore resources in the LA region" link="/ImmigrationResources" image={resourceimage} />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init Job', () => {
    const job = {
        name: 'TechJob',
        company: 'Facebook',
        categoryNames: 'Software Engineer',
        formattedDate: '4/20/1969',
        locationNames: 'San Dimas, CA',
        jobInstancesURL: 'google.com',
    }
    const component = renderer.create(
        <BrowserRouter>
            <JobCard jobsData={job} />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init House', () => {
    const house = {
        property_name_text: 'Monster House',
        is_under_management_ind: 'YES',
        has_active_financing_ind: 'YES',
        people_per_unit: '2',
        total_avbl_units: '1',
        rent_per_month: '420',
        id: '69',
    }
    const component = renderer.create(
        <BrowserRouter>
            <HouseCard homeData={house} />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init Resource', () => {
    const resource = {
        description: 'a good thing',
        name: 'the good place',
        hours: 'Sunday-Saturday: Closed',
        addrln1: '1225 North Pole',
        url: 'google.com',
        id: '69',
    }
    const component = renderer.create(
        <BrowserRouter>
            <ResourceCard resourceData={resource} />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init CharityCard', () => {
    const component = renderer.create(
        <BrowserRouter>
            <CharityCard name=""
                image={immigrantdefenders}
                link="https://www.immdef.org/" />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init ToolCard', () => {
    const component = renderer.create(
        <BrowserRouter>
            <ToolCard name=""
                image={mirys} />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init NavBar', () => {
    const component = renderer.create(
        <BrowserRouter>
            <Navbar />
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

it('Init MemberCard', () => {
    const props = {
        name: 'John Doe', // Provide a name here
        bio: 'A software engineer',
        responsibilities: 'Developer',
        gitID: 'johndoe',
        commits: 100,
        issues: 5,
        tests: 20,
        image: 'path_to_image.jpg'
    };
    const component = renderer.create(
        <BrowserRouter>
            <MemberCard {...props}></MemberCard>
        </BrowserRouter>,
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
