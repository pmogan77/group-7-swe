import React from "react";
import { useParams } from 'react-router-dom';
import { useState, useEffect } from "react";

// Jobs
import JobCard from '../components/JobCard'
// Resources
import ResourceCard from "../components/ResourceCard";


import Container from "react-bootstrap/Container";
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";
import "../css-styles/Instance.css"
import Map from "../components/Map"
import axios from "axios";

export default function HousingInstance() {

    const { houseId } = useParams(); // Gets current job ID by passing it in
    const [houseData, setHouseData] = useState([]);
    const [nearbyJobs, setNearbyJobs] = useState([]);
    const [nearbyImmigration, setNearbyImmigration] = useState([]);


    const houseDataURL = `https://api.la-integration-initiative.me/house/${houseId}`;
    const nearbyJobsURL = `https://api.la-integration-initiative.me/house/${houseId}/nearbyJobs`;
    const nearbyImmigrationURL = `https://api.la-integration-initiative.me/house/${houseId}/nearbyImmigration`;


    useEffect(() => {
        async function getAllData() {
            try {
                const houseDataResponse = await axios.get(houseDataURL);
                const nearbyJobsResponse = await axios.get(nearbyJobsURL);
                const nearbyImmigrationResponse = await axios.get(nearbyImmigrationURL);
                setNearbyImmigration(nearbyImmigrationResponse.data.immigrations);
                setNearbyJobs(nearbyJobsResponse.data.jobs);
                setHouseData(houseDataResponse.data);


            } catch (error) {
                console.error('Error fetching job data:', error);
            }
        }
        getAllData();

    }, [houseId, houseDataURL, nearbyJobsURL, nearbyImmigrationURL]);


    const {

        property_name_text,
        is_under_management_ind,
        has_active_financing_ind,
        people_per_unit,
        total_avbl_units,
        rent_per_month,
        mgmt_agent_org_name,
        mgmt_contact_full_name,
        mgmt_contact_address_line1,
        mgmt_contact_city_name,
        mgmt_contact_state_code,
        mgmt_contact_zip_code,
        mgmt_contact_main_phn_nbr,
        mgmt_contact_email_text,
        lat,
        lon
    } = houseData;

    var minPeoplePerUnit = Math.floor(people_per_unit);
    var maxPeoplePerUnit = Math.ceil(people_per_unit);
    var people_per_unit_range = people_per_unit;


    //convert people per unit to a range if number is not a whole number
    if (minPeoplePerUnit !== people_per_unit || maxPeoplePerUnit !== people_per_unit) {
        people_per_unit_range = String(minPeoplePerUnit + "-" + maxPeoplePerUnit);
    }

    let mapComponent = null;

    if (lat !== undefined && lon !== undefined) {
        mapComponent = <Map center={[lon, lat]} />;
    } else {
        mapComponent = <div>Loading Map...</div>;
    }

    return (
        <Container className="main-container">
            <Container className="attributes-again">
                <h1>{property_name_text}</h1>
                <p className="under-managment">{"Under Management: " + is_under_management_ind}</p>
                <p className="active-financing">{"Has Active Financing: " + has_active_financing_ind}</p>
                <p className="people-per-unit">{"People Per Unit: " + people_per_unit_range}</p>
                <p className="total-avail">{"Total Available Units: " + total_avbl_units}</p>
                <p className="rent">{"Rent: $" + rent_per_month}</p>
            </Container>

            <Container className="management">
                <h3>Management Information</h3>
                <p>{"Management Organization Name: " + mgmt_agent_org_name}</p>
                <p>{"Management Contact: " + mgmt_contact_full_name}</p>
                <p>{"Management Address: " + mgmt_contact_address_line1 + " "
                    + mgmt_contact_city_name + ", " + mgmt_contact_state_code + " " + mgmt_contact_zip_code}</p>
                <p>{"Phone Number: " + mgmt_contact_main_phn_nbr}</p>
                <p>{"Email: " + mgmt_contact_email_text}</p>
                <div className="map-image">
                    <h1>Map: </h1>
                    {mapComponent}
                </div>


            </Container>
            <Container className="other-instances">
                <h3> Nearby Jobs</h3>
                <Container className="card-holder">
                    {/* <JobCard jobsData={jobsData} num={0} />
                    <JobCard jobsData={jobsData} num={1} /> */}
                    {nearbyJobs.slice(0, 4).map((job, index) => {
                        return <JobCard key={index} jobsData={nearbyJobs} num={index} />;
                    })}

                </Container>
                <h3>Nearby Resources</h3>
                <Container className="card-holder">
                    {nearbyImmigration.slice(0, 4).map((house, index) => {
                        return <ResourceCard key={index} resourceData={nearbyImmigration} num={index} />;
                    })}
                </Container>
            </Container>
            <Container className="back-button">
                <Link to="/AffordableHousing">
                    <Button variant="primary">Back To Housing</Button>
                </Link>
            </Container>
        </Container>
    );
}
