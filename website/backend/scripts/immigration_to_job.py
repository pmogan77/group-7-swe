import json
import random
import mysql.connector
from math import radians, cos, sin, asin, sqrt

from pathlib import Path

path = Path(__file__).parent

import os

from dotenv import load_dotenv
load_dotenv()

# Load the jobs dataset, immigration services dataset, and city lat/lon mapping from JSON files
with open(path/"filtered_jobs.json", 'r') as jobs_file:
    jobs_data = json.load(jobs_file)['results']

with open(path/"immigration.json", 'r') as immigration_file:
    immigration_data = json.load(immigration_file)['features']

with open(path/"city.json", 'r') as city_file:
    city_data = json.load(city_file)


# Function to calculate the distance between two points using Haversine formula
def calculate_distance(lat1, lon1, lat2, lon2):
    # Radius of the Earth in miles
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2

    c = 2 * asin(sqrt(a))

    # Radius of the earth in kilometers. Use 3956 for miles
    r = 6371

    # calculate the result
    return c * r * 0.621371

# Function to perform case-insensitive substring search
def case_insensitive_substring(city, text):
    return city.lower() in text.lower()

# Function to get jobs near an immigration service within a specified distance
def get_jobs_near_immigration_service(service_lat, service_lon, jobs_data, city_data, radius):
    nearby_jobs = []
    for job in jobs_data:
        job_name = job['name']
        job_lat = 34.0522
        job_lon = -118.2437

        # Check if any city substring is a case-insensitive match in the job's name
        for city_info in city_data:
            if case_insensitive_substring(city_info['city'], job_name):
                job_lat = city_info['lat']
                job_lon = city_info['long']
                break

        # Calculate the distance between the immigration service and the job
        distance = calculate_distance(service_lat, service_lon, job_lat, job_lon)

        if distance <= radius:  # Check if the job is within the specified distance
            nearby_jobs.append(job['id'])  # Append job ID to the list

    return nearby_jobs

conn = mysql.connector.connect(
    host=os.environ.get("sql_host"),
    user=os.environ.get("sql_username"),
    password=os.environ.get("sql_password"),
    database=os.environ.get("sql_database")
)
cursor = conn.cursor()

# Iterate through each immigration service in the dataset
for service in immigration_data:
    service_lat = service['geometry']['y']
    service_lon = service['geometry']['x']
    service_id = service['attributes']['OBJECTID']

    # Find jobs within a specified distance of the immigration service
    nearby_jobs = get_jobs_near_immigration_service(service_lat, service_lon, jobs_data, city_data, 20)

    if len(nearby_jobs) < 3:
        nearby_jobs = get_jobs_near_immigration_service(service_lat, service_lon, jobs_data, city_data, 49)

    if len(nearby_jobs) > 5:
        nearby_jobs = random.sample(nearby_jobs, 5)

    if len(nearby_jobs) > 0:
        # Insert immigration service ID and job IDs into the MySQL table
        for job_id in nearby_jobs:
            cursor.execute('INSERT INTO immigration_to_job (job_id, immigration_id) VALUES (%s, %s)', (job_id, service_id))

    # Commit changes to the MySQL database
    conn.commit()

    print(f"Immigration Service ID: {service_id}")
    for job_id in nearby_jobs:
        print("\tJob ID:", job_id)

# Close the MySQL database connection
conn.close()