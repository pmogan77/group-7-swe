import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import { members } from "../TeamInfo";
import { tools } from "../ToolsInfo";
import { sources } from "../SourcesInfo";
import axios from "axios";
import MemberCard from "../components/MemberCard";
import "../css-styles/About.css";
import ToolCard from "../components/ToolCard";
import CharityCard from "../components/CharityCard";
import mirys from "../media/miryslist-logo.jpg";
import immigrantdefenders from "../media/immigrantdefenders-logo.jpg";
import parsequality from "../media/parsequality-logo.jpg";

export default function About() {
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);

  async function getCommits() {
    let response = await axios.get(
      "https://gitlab.com/api/v4/projects/50452743/repository/contributors"
    );
    let data = await response.data;
    var commitSum = 0;
    setTotalCommits(0);
    for (let i = 0; i < data.length; i++) {
      // convert to for loop
      for (let j = 0; j < members.length; j++) {
        //add commit count to members commit if match in name
        if (data[i].name === members[j].name[0] || data[i].name === members[j].name[1]) {
          members[j].commits = data[i].commits;
          
        }
      }
      
    }
    for (let j = 0; j < members.length; j++) {
      commitSum += members[j].commits;
    }


    setTotalCommits(commitSum);
  }

  async function getIssues() {
    let response = await axios.get(
      "https://gitlab.com/api/v4/projects/50452743/issues"
    );
    let data = await response.data;
    var issueSum = 0;
    members.forEach((member) => {
      var memberSum = 0;
      for (let i = 0; i < data.length; i++) {
        //add commit count to members commit if match in name
        if (data[i].author.name === member.name[0] || data[i].author.name === member.name[1]) {
          memberSum += 1;
          issueSum += 1;
        }
      }
      member.issues = memberSum;
    });

    setTotalIssues(issueSum);
  }

  useEffect(() => {
    getCommits();
    getIssues();
  }, []);

  return (
    <Container id="about-us-container">
      <h1>About Us</h1>
      <p className="about-us-text">
        LA Integration Initiative aims to seamlessly integrate the lives of
        immigrants into the Los Angeles community. If you are looking for
        affordable housing, jobs or immigration services, turn to our website to
        access all this information in one place!
      </p>
      <p className="about-us-text">
        Our project serves immigrants within Los Angeles. We will provide
        additional resources to help address job security, affordable housing,
        and immigration resources.This will help immigrants coming into LA by
        providing them the resources they need to integrate into their new lives
        seamlessly.
      </p>
      <p className="about-us-text">
        Los Angeles has 4.4 million immigrants, representing 33% of the
        population of the city. In addition, this group has been expanding over
        time. In order to help this group of people adapt to their lives within
        the LA region, we are providing resources that can help them integrate
        into the community.
      </p>

      <h1>Interesting Results</h1>
      <p className="about-us-text">
        After integrating data from the Los Angeles Housing Department, job data
        api, and the immigration resources, we found it interesting to see how
        tightly connected and close all the different instances of each model
        were. We were also surprised by the number of resources around the LA
        area for immigrants.
      </p>

      <h1>Meet The Team!</h1>
      <div className="member-cards">
        {members.map((member) => {
          return <MemberCard {...member} />;
        })}
      </div>

      <h1>Totals</h1>
      <div className="totals-text">
        <p>Total Commits: {totalCommits}</p>
        <p>Total Issues: {totalIssues}</p>
      </div>

      <Container>
        <br></br>
        <br></br>
        <h1>Tools Used</h1>

        <div className="tool-cards">
          {tools.map((name) => {
            return <ToolCard {...name} />;
          })}
        </div>
      </Container>

      <Container>
        <br></br>
        <br></br>
        <h1>Sources Used</h1>

        <div className="source-cards">
          {sources.map((name) => {
            return <ToolCard {...name} />;
          })}
        </div>
        
        <br></br>

        <a
          href="https://gitlab.com/pmogan77/group-7-swe"
          target="_blank"
          rel="noreferrer"
          className="custom1"
          style={{ color: 'blue' }}
        >
          <h2 className="api-doc-text" >GitLab Repository</h2>
        </a>

        <br></br>
        <br></br>
        <a
          href="https://documenter.getpostman.com/view/14095754/2s9YJXYQNV"
          target="_blank"
          rel="noreferrer"
          className="custom1"
          style={{ color: 'blue' }}
        >
          <h2 className="api-doc-text" >API Documentation</h2>
        </a>

        <br></br>
        <br></br>
        <h1>Want To Help?</h1>

        <p className="donate-text">
          Check out these resources to learn how to help:
        </p>
        <div className="charities">
          <CharityCard
            name=""
            image={mirys}
            link="https://miryslist.org/"
            bio="Miry's List is a nonprofit organization providing a mechanism for people to directly help new arrival refugee families with the incredible challenge of resettlement in the United States. 
            "
          ></CharityCard>
          <CharityCard
            name=""
            image={immigrantdefenders}
            link="https://www.immdef.org/"
            bio="Immigrant Defenders is fighting back every day against the immigration system's campaign of cruelty against migrants at the border with a focus on assisting children and families.
            "
          ></CharityCard>
          <CharityCard
            name=""
            image={parsequality}
            link="https://parsequalitycenter.org/what-we-do/social-services/los-angeles-center/?doing_wp_cron=1686923659.0686240196228027343750"
            bio="Pars Equality Center’s mission is to catalyze social, civic, and economic integration of immigrants from underrepresented communities into American society.
            "
          ></CharityCard>
        </div>
      </Container>
    </Container>
  );
}
