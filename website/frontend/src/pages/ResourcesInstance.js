import React from "react";
import { useParams } from 'react-router-dom';
import { useState, useEffect } from "react";
import axios from "axios";
// House
import HouseCard from '../components/HouseCard'
// Jobs
import JobCard from '../components/JobCard'
import Container from "react-bootstrap/Container";
import "../css-styles/Instance.css"
import Map from "../components/Map"
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";

export default function ResourcesInstance() {

    const { resourceId } = useParams(); // Gets current job ID by passing it in
    const [resourceData, setResourceData] = useState([]);
    const [nearbyHouses, setNearbyHouses] = useState([]);
    const [nearbyJobs, setNearbyJobs] = useState([]);

    const resourceDataURL = `https://api.la-integration-initiative.me/immigration/${resourceId}`;
    const nearbyHousesURL = `https://api.la-integration-initiative.me/immigration/${resourceId}/nearbyHouses`;
    const nearbyJobsURL = `https://api.la-integration-initiative.me/immigration/${resourceId}/nearbyJobs`;

    useEffect(() => {
        async function getAllData() {
            try {
                const resourceDataResponse = await axios.get(resourceDataURL);
                const nearbyHousesResponse = await axios.get(nearbyHousesURL);
                const nearbyJobsResponse = await axios.get(nearbyJobsURL);
                setNearbyJobs(nearbyJobsResponse.data.jobs);
                setNearbyHouses(nearbyHousesResponse.data.houses);
                setResourceData(resourceDataResponse.data);
            } catch (error) {
                console.error('Error fetching job data:', error);
            }
        }



        getAllData();

    }, [resourceId, nearbyJobsURL, nearbyHousesURL, resourceDataURL]);



    const {
        description,
        name,
        hours,
        addrln1,
        url,
        describe,
        latitude,
        longitude
    } = resourceData;

    let mapComponent = null;

    if (latitude !== undefined && longitude !== undefined) {
        mapComponent = <Map center={[longitude, latitude]} />;
    } else {
        mapComponent = <div>Loading Map...</div>;
    }


    const urlWithProtocol = "https://" + url;

    return (
        <Container className="main-container">
            <Container className="attributes-again">
                <h1>{name}</h1>
                <p className="description">{description}</p>
                <p className="hours">{"Hours: " + (hours !== null ? hours : "N/A")} </p>
                <p className="address">{"Address: " + addrln1}</p>
                <p className="url"><a href={urlWithProtocol}>Link To Resource</a></p>
            </Container>
            <Container className="job-description">
                <div className="map-image">
                    <h1>Map: </h1>

                    {mapComponent}
                </div>
                <div dangerouslySetInnerHTML={{ __html: describe }} />
            </Container>
            <Container className="other-instances">
                <h3>Nearby Housing</h3>
                <Container className="card-holder">

                    {nearbyHouses.slice(0, 4).map((house, index) => {
                        return <HouseCard key={index} homeData={nearbyHouses} num={index} />;
                    })}
                </Container>
                <h3> Nearby Jobs</h3>
                <Container className="card-holder">
                    {nearbyJobs.slice(0, 4).map((job, index) => {
                        return <JobCard key={index} jobsData={nearbyJobs} num={index} />;
                    })}
                </Container>
            </Container>
            <Container className="back-button">
                <Link to="/ImmigrationResources">
                    <Button variant="primary">Back To Resources</Button>
                </Link>
            </Container>
        </Container>
    );
}
