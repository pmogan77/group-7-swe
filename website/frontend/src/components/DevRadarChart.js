import React from 'react';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis } from 'recharts';
import effect_data from "../visualization/drug_side_effect.json"

function SimpleRadarChart() {

    const transformDataForRadarChart = (data) => {
        return data.map(item => ({
            subject: item.subject,
            value: item.A, // Assuming "A" is the value you want to use for the radar chart
        }));
    };

    const data = transformDataForRadarChart(effect_data);

    return (
        <RadarChart width={800} height={600} margin={{ top: 20, right: 30, bottom: 20, left: 30 }} data={data}>
            <PolarGrid />
            <PolarAngleAxis dataKey="subject" />
            <PolarRadiusAxis angle={30} domain={[0, 2]} />
            <Radar dataKey="value" fill="#8884d8" fillOpacity={0.6} />
        </RadarChart>
    );
};

export default SimpleRadarChart;
