import React from "react";
import { Route, Routes } from "react-router-dom";
import NavBar from "./Navbar";
import Home from "./pages/Home";
import About from "./pages/About";
import Jobs from "./pages/Jobs";
import Search from "./pages/Search";
import Visualization from "./pages/Visualization";
import DevViz from "./pages/DevViz";
import AffordableHousing from "./pages/AffordableHousing";
import ImmigrationResources from "./pages/ImmigrationResources";
import JobInstance from "./pages/JobInstance";
import "./css-styles/App.css";
import HousingInstance from "./pages/HousingInstance";
import ResourcesInstance from "./pages/ResourcesInstance";
import { useState } from 'react';

const App = () => {

  // To hold the search query in the navbar so that it can be accesses in the search page
  const [searchQuery, setSearchQuery] = useState('');

  return (
    <>
      <NavBar setSearchQuery={setSearchQuery} />
      <div className="app-container">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Home" element={<Home />} />
          <Route path="/About" element={<About />} />
          <Route path="/Jobs" element={<Jobs />} />
          <Route path="/AffordableHousing" element={<AffordableHousing />} />
          <Route
            path="/ImmigrationResources"
            element={<ImmigrationResources />}
          />
          <Route path="/DevViz" element={<DevViz />} />
          <Route path="/Visualization" element={<Visualization />} />
          <Route path="/JobInstance/:jobId" element={<JobInstance />} />
          <Route path="/HousingInstance/:houseId" element={<HousingInstance />} />
          <Route path="/ResourcesInstance/:resourceId" element={<ResourcesInstance />} />
          <Route path="/Search" element={<Search searchQuery={searchQuery} />} />
        </Routes>
      </div>
    </>
  );
};

export default App;
