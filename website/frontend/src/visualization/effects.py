import requests
import json

api_url = "http://3.144.76.176:8080/get_effects"

# Fetch data from the API
response = requests.get(api_url)
data = response.json()

# Create a dictionary to store severity frequency count
severity_count = {}

# Process the data and count the frequency of each severity level
for entry in data:
    severity = entry["severity"]
    severity_count[severity] = severity_count.get(severity, 0) + 1

# Create a JSON file with the severity frequency count
output_file_path = "severity_frequency_effects.json"
with open(output_file_path, "w") as json_file:
    json.dump(severity_count, json_file, indent=2)

print(f"Severity frequency count for effects has been saved to {output_file_path}")