import { React, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Container from "react-bootstrap/Container";
import "../css-styles/Dropdown.css"



const CustomDropdown = ({ title, items, setterFunc, scroll }) => {
  function handleClick(value) {
    setSelectedOption(value);
    setterFunc(value);
  }

  const [selectedOption, setSelectedOption] = useState(title);

  return (
    <DropdownButton
      id="dropdown-basic-button"
      className="dropdown-button"
      title={selectedOption}
    >
      <Container style={scroll ? { height: "20rem", overflowY: "scroll" } : {}}>
        {items.map((item) => {
          return (
            <Dropdown.Item onClick={() => handleClick(item)}>
              {item}
            </Dropdown.Item>
          );
        })}
      </Container>
    </DropdownButton>
  );
};

export default CustomDropdown;
