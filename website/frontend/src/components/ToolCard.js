import React from "react";
import Card from "react-bootstrap/Card";
import "../css-styles/ToolCard.css";

function ToolCard({ name, image }) {
  return (
    <Card style={{ width: "18rem" }} id="tool-box">
      <Card.Img variant="top" src={image} />
      <Card.Body>
        <Card.Title id="tools-card-title"> {name}</Card.Title>
      </Card.Body>
    </Card>
  );
}

export default ToolCard;
