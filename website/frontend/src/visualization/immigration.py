import requests
import json

api_url_base = 'https://api.la-integration-initiative.me/immigration'

# Function to fetch data from the API for a given page and per_page
def fetch_data(page, per_page=2):
    try:
        response = requests.get(f'{api_url_base}?page={page}&per_page={per_page}')
        data = response.json()
        return data
    except Exception as e:
        print(f'Error fetching data for page {page}: {e}')
        return None

# Function to get the total number of pages
def get_total_pages(total_objects, per_page):
    total_pages = (total_objects + per_page - 1) // per_page
    return total_pages

# Function to process the data and generate zip code to frequency mapping
def process_zip_code_data(data, zip_code_counts):
    if not data or not data.get('results') or len(data['results']) == 0:
        print('Invalid data format or empty results.')
        return

    # Count occurrences of each zip code
    for item in data['results']:
        zip_code = str(item['zip'])
        zip_code_counts[zip_code] = zip_code_counts.get(zip_code, 0) + 1

# Function to write the result to a JSON file
def write_to_json_file(data, filename='zip_code_counts.json'):
    with open(filename, 'w') as file:
        json.dump(data, file, indent=2)

# Main execution
def main():
    # Make an initial call to get information about total objects and items per page
    initial_data = fetch_data(1)
    print(initial_data)
    total_objects = initial_data.get('total_immigrations', 0)
    per_page = initial_data.get('per_page', 2)

    if total_objects == 0:
        print('Failed to retrieve total objects information. Exiting.')
        return

    total_pages = get_total_pages(total_objects, per_page)
    zip_code_counts = {}

    for page in range(1, total_pages + 1):
        api_data = fetch_data(page, per_page)
        process_zip_code_data(api_data, zip_code_counts)

    # Write the final result to a JSON file
    write_to_json_file(zip_code_counts)

# Run the script
if __name__ == '__main__':
    main()
