import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Link } from 'react-router-dom';
import { Highlight } from 'react-highlight-regex'
import "../css-styles/Card.css";

function HouseCard({ homeData, num, highlight = null }) {
  const houseData = num !== undefined ? homeData[num] : homeData;

  if (!houseData) {
    return <div>House not found</div>;
  }

  const {
    property_name_text,
    is_under_management_ind,
    has_active_financing_ind,
    people_per_unit,
    total_avbl_units,
    rent_per_month,
    id
  } = houseData;

  const minPeoplePerUnit = Math.floor(people_per_unit);
  const maxPeoplePerUnit = Math.ceil(people_per_unit);
  const people_per_unit_range = (minPeoplePerUnit !== people_per_unit || maxPeoplePerUnit !== people_per_unit)
    ? `${minPeoplePerUnit}-${maxPeoplePerUnit}`
    : String(people_per_unit);

  const houseId = id;
  const houseInstancesURL = `/HousingInstance/${houseId}`;

  const highlightText = (input) => {
    if (highlight) {
      const highlightRegex = new RegExp(highlight, "i");
      return <Highlight highlightClassname={"mark"} match={highlightRegex} text={input} />;
    }
    return input;
  };

  return (
    <Card style={{ width: "16rem" }} id="full-card">
      <Card.Body id="card-body">
        <Container className="card-info">
          <Card.Title id="title">{highlightText(property_name_text)}</Card.Title>
          <Card.Text>{highlightText(`Is Under Management: ${is_under_management_ind}`)}</Card.Text>
          <Card.Text>{highlightText(`Has Active Financing: ${has_active_financing_ind}`)} </Card.Text>
          <Card.Text> {highlightText(`People Per Unit: ${people_per_unit_range}`)}</Card.Text>
          <Card.Text> {highlightText(`Total Available Units: ${total_avbl_units}`)}</Card.Text>
          <Card.Text> {highlightText(`Rent Per Month: $${rent_per_month}`)} </Card.Text>
          <div className="button-container">
            <Link to={houseInstancesURL}>
              <Button variant="light">Learn More</Button>
            </Link>
          </div>
        </Container>
      </Card.Body>
    </Card>
  );
}

export default HouseCard;
