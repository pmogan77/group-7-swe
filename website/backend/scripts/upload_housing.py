import mysql.connector
import json

import os

from dotenv import load_dotenv
load_dotenv()

def connect_to_database():
    connection = mysql.connector.connect(
        host=os.environ.get("sql_host"),
        user=os.environ.get("sql_username"),
        password=os.environ.get("sql_password"),
        database=os.environ.get("sql_database")
    )
    return connection


def insert_immigration_data(data):
    connection = connect_to_database()
    cursor = connection.cursor()

    for result in data["results"]:
        # Extract data from JSON
        id = result["PROPERTY_ID"]
        PROPERTY_NAME_TEXT = result["PROPERTY_NAME_TEXT"]
        HAS_ACTIVE_FINANCING_IND = result["HAS_ACTIVE_FINANCING_IND"]
        IS_UNDER_MANAGEMENT_IND = result["IS_UNDER_MANAGEMENT_IND"]
        CNTY_NM2KX = result["CNTY_NM2KX"]
        LAT = result["LAT"]
        LON = result["LON"]
        MGMT_AGENT_ORG_NAME = result["MGMT_AGENT_ORG_NAME"]
        MGMT_CONTACT_FULL_NAME = result["MGMT_CONTACT_FULL_NAME"]
        MGMT_CONTACT_ADDRESS_LINE1 = result["MGMT_CONTACT_ADDRESS_LINE1"]
        MGMT_CONTACT_CITY_NAME = result["MGMT_CONTACT_CITY_NAME"]
        MGMT_CONTACT_STATE_CODE = result["MGMT_CONTACT_STATE_CODE"]
        MGMT_CONTACT_ZIP_CODE = result["MGMT_CONTACT_ZIP_CODE"]
        MGMT_CONTACT_MAIN_PHN_NBR = result["MGMT_CONTACT_MAIN_PHN_NBR"]
        MGMT_CONTACT_EMAIL_TEXT = result["MGMT_CONTACT_EMAIL_TEXT"]
        TOTAL_AVBL_UNITS = result["TOTAL_AVBL_UNITS"]
        PEOPLE_PER_UNIT = result["PEOPLE_PER_UNIT"]
        RENT_PER_MONTH = result["RENT_PER_MONTH"]

        # Define the INSERT query
        insert_query = """
    INSERT INTO housing (
        id, PROPERTY_NAME_TEXT, HAS_ACTIVE_FINANCING_IND,
        IS_UNDER_MANAGEMENT_IND, LAT, LON,
        MGMT_AGENT_ORG_NAME, MGMT_CONTACT_FULL_NAME,
        MGMT_CONTACT_ADDRESS_LINE1, MGMT_CONTACT_CITY_NAME,
        MGMT_CONTACT_STATE_CODE, MGMT_CONTACT_ZIP_CODE,
        MGMT_CONTACT_MAIN_PHN_NBR, MGMT_CONTACT_EMAIL_TEXT,
        TOTAL_AVBL_UNITS, PEOPLE_PER_UNIT, RENT_PER_MONTH
    )
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
"""

        # Execute the INSERT query
        cursor.execute(
            insert_query,
            (
                id,
                PROPERTY_NAME_TEXT,
                HAS_ACTIVE_FINANCING_IND,
                IS_UNDER_MANAGEMENT_IND,
                LAT,
                LON,
                MGMT_AGENT_ORG_NAME,
                MGMT_CONTACT_FULL_NAME,
                MGMT_CONTACT_ADDRESS_LINE1,
                MGMT_CONTACT_CITY_NAME,
                MGMT_CONTACT_STATE_CODE,
                MGMT_CONTACT_ZIP_CODE,
                MGMT_CONTACT_MAIN_PHN_NBR,
                MGMT_CONTACT_EMAIL_TEXT,
                TOTAL_AVBL_UNITS,
                PEOPLE_PER_UNIT,
                RENT_PER_MONTH,
            ),
        )

    # Commit the changes and close the connection
    connection.commit()
    cursor.close()
    connection.close()


def read_json_data():
    from pathlib import Path

    path = Path(__file__).parent
    with open(path/"filtered_housing.json", "r", encoding="utf8") as json_file:
        data = json.load(json_file)
    return data


if __name__ == "__main__":
    data_to_insert = read_json_data()
    insert_immigration_data(data_to_insert)
