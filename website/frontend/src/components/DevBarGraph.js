import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import effect_data from "../visualization/effect_severity.json";


function DevBarGraph() {

    const data = Object.entries(effect_data).map(([zip, count]) => ({
        zip: String(zip),
        count: Number(count)
    }));

    return (
        <BarChart width={800} height={400} data={data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="zip" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar type="monotone" dataKey="count" stroke="#8884d8" fill="#8051A0" />
        </BarChart>
    );
}


export default DevBarGraph;
