import json
from math import radians, cos, sin, asin, sqrt
import random
import mysql.connector

from pathlib import Path

path = Path(__file__).parent

import os

from dotenv import load_dotenv
load_dotenv()

# Load the housing dataset, jobs dataset, and city lat/lon mapping from JSON files
with open(path / "filtered_housing.json", "r") as housing_file:
    housing_data = json.load(housing_file)

with open(path / "filtered_jobs.json", "r") as jobs_file:
    jobs_data = json.load(jobs_file)["results"]

with open(path / "city.json", "r") as city_file:
    city_data = json.load(city_file)


# Function to calculate the distance between two points using Haversine formula
def calculate_distance(lat1, lon1, lat2, lon2):
    # Radius of the Earth in miles
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2

    c = 2 * asin(sqrt(a))

    # Radius of earth in kilometers. Use 3956 for miles
    r = 6371

    # calculate the result
    return c * r * 0.621371


# Function to perform case-insensitive substring search
def case_insensitive_substring(city, text):
    return city.lower() in text.lower()


def get_jobs_near_house(house_lat, house_lon, jobs_data, city_data, radius):
    nearby_jobs = []
    for job in jobs_data:
        job_name = job["name"]
        job_lat = 34.0522
        job_lon = -118.2437

        # Check if any city substring is a case-insensitive match in the job's name
        for city_info in city_data:
            if case_insensitive_substring(city_info["city"], job_name):
                job_lat = city_info["lat"]
                job_lon = city_info["long"]
                break

        # Calculate the distance between the house and the job
        distance = calculate_distance(house_lat, house_lon, job_lat, job_lon)

        if distance <= radius:  # Check if the job is within 20 miles
            nearby_jobs.append(job)

    return nearby_jobs


# Create a connection to the MySQL database
conn = mysql.connector.connect(
    host=os.environ.get("sql_host"),
    user=os.environ.get("sql_username"),
    password=os.environ.get("sql_password"),
    database=os.environ.get("sql_database")
)

cursor = conn.cursor()

# Iterate through each house in the housing dataset
for house in housing_data["results"]:
    house_lat = house["LAT"]
    house_lon = house["LON"]
    house_id = house["PROPERTY_ID"]

    # Find jobs within 20 miles of the house
    nearby_jobs = get_jobs_near_house(house_lat, house_lon, jobs_data, city_data, 20)

    if len(nearby_jobs) < 3:
        nearby_jobs = get_jobs_near_house(
            house_lat, house_lon, jobs_data, city_data, 40
        )

    if len(nearby_jobs) > 5:
        # pick a random 5, actually random
        nearby_jobs = random.sample(nearby_jobs, 5)

    for job in nearby_jobs:
        job_id = job["id"]
        cursor.execute(
            "INSERT INTO house_to_job (house_id, job_id) VALUES (%s, %s)",
            (house_id, job_id),
        )

    # Commit changes to the MySQL database
    conn.commit()

    print(house_id)

    for job in nearby_jobs:
        print("\t", job["id"])
