import React from "react";
import { useParams } from 'react-router-dom';
import { useState, useEffect } from "react";
import HouseCard from '../components/HouseCard'
import axios from "axios";
import ResourceCard from "../components/ResourceCard";
import Container from "react-bootstrap/Container";
import "../css-styles/Instance.css"
import Map from "../components/Map"
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";
import JobDescription from "../components/DescriptionBox"

export default function JobInstance() {

    const { jobId } = useParams(); // Gets current job ID by passing it in
    const [jobData, setJobData] = useState([]);
    const [nearbyHouses, setNearbyHouses] = useState([]);
    const [nearbyImmigration, setNearbyImmigration] = useState([]);


    // Hard coded map to be focused at random location in LA for phase1
    const LON = -118;
    const LAT = 34.05;

    const jobDataURL = `https://api.la-integration-initiative.me/job/${jobId}`;
    const nearbyHousesURL = `https://api.la-integration-initiative.me/job/${jobId}/nearbyHouses`;
    const nearbyImmigrationURL = `https://api.la-integration-initiative.me/job/${jobId}/nearbyImmigration`;



    useEffect(() => {
        async function getAllData() {
            try {
                const jobDataResponse = await axios.get(jobDataURL);
                const nearbyHousesResponse = await axios.get(nearbyHousesURL);
                const nearbyImmigrationResponse = await axios.get(nearbyImmigrationURL);
                setNearbyHouses(nearbyHousesResponse.data.houses);
                setNearbyImmigration(nearbyImmigrationResponse.data.immigrations);
                setJobData(jobDataResponse.data);
            } catch (error) {
                console.error('Error fetching job data:', error);
            }
        }

        getAllData();


    }, [jobId, nearbyHousesURL, jobDataURL, nearbyImmigrationURL]);

    const { name, categories, publication_date, locations, company, refs, describe, contents } = jobData;

    const url = refs;

    // Format the date to look pretty
    const formattedDate = new Date(publication_date).toLocaleDateString();

    // maps all the locations and categories
    let locationNames = '';
    if (Array.isArray(locations)) {
        locationNames = locations.map(location => location.name).join(', ');
    } else if (locations) {
        locationNames = locations;
    }

    let categoryNames = '';
    if (Array.isArray(categories)) {
        categoryNames = categories.map(category => category.name).join(', ');
    } else if (categories) {
        categoryNames = categories;
    }

    return (
        <Container className="main-container">
            <Container className="attributes-again">
                <h1>{name}</h1>
                <p className="Company">{"Company: " + company}</p>
                <p className="Category">{"Category: " + categoryNames}</p>
                <p className="Published">{"Publish Date: " + formattedDate}</p>
                <p className="Location">{"Location: " + locationNames}</p>
                <p className="url"><a href={url}>Link To Job Posting</a></p>
            </Container>
            <Container className="job-description">
                <div dangerouslySetInnerHTML={{ __html: describe }} />
                <div className="map-image">
                    <h1>Map: </h1>
                    <Map center={[LON, LAT]} ></Map>
                </div>
            </Container>
            <JobDescription contents={contents} />
            {/* <Container className="job-text">
            </Container> */}
            <Container className="other-instances">
                <h3>Nearby Housing</h3>
                <Container className="card-holder">
                    {nearbyHouses.slice(0, 4).map((house, index) => {
                        return <HouseCard key={index} homeData={nearbyHouses} num={index} />;
                    })}

                </Container>
                <h3>Nearby Resources</h3>
                <Container className="card-holder">
                    {nearbyImmigration.slice(0, 4).map((resource, index) => {
                        return <ResourceCard key={index} resourceData={nearbyImmigration} num={index} />;
                    })}
                </Container>
            </Container>
            <Container className="back-button">
                <Link to="/Jobs">
                    <Button variant="primary">Back To Jobs</Button>
                </Link>
            </Container>
        </Container>
    );
}
