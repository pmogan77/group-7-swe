import { Link, useMatch, useResolvedPath, useNavigate } from "react-router-dom";
import React, { useState } from 'react';
import "./css-styles/Navbar.css";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function NavBar({ setSearchQuery }) {
  const [search, setSearch] = useState('');
  const navigate = useNavigate();

  const handleSearch = (e) => {
    e.preventDefault();
    setSearchQuery(search);
    navigate(`/Search`);
  };

  return (
    <Navbar expand="xxl" className="bg-body mb-0 pb-0 pt-0">
      <nav className="nav">
        <Link to="/" className="site-title">
          LA Integration Initiative
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav" className="custom-collapse">
          <Nav className="navbar-nav mb-2 mb-lg-0">
            <Nav.Item>
              <CustomLink to="/Home">Home</CustomLink>
            </Nav.Item>
            <Nav.Item>
              <CustomLink to="/About">About</CustomLink>
            </Nav.Item>
            <Nav.Item>
              <CustomLink to="/Jobs">Jobs</CustomLink>
            </Nav.Item>
            <Nav.Item>
              <CustomLink to="/AffordableHousing">Affordable Housing</CustomLink>
            </Nav.Item>
            <Nav.Item>
              <CustomLink to="/ImmigrationResources">
                Immigration Resources
              </CustomLink>
            </Nav.Item>
            <Nav.Item>
              <CustomLink to="/Visualization">
                Visualization
              </CustomLink>
            </Nav.Item>
            <Nav.Item>
              <CustomLink to="/DevViz">
                Developer Visualization
              </CustomLink>
            </Nav.Item>
          </Nav>
          <Form className="d-flex" onSubmit={handleSearch}>
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            />
            <CustomLink to={`/Search`}>
              <Button variant="outline-light" onClick={handleSearch}>Search</Button>
            </CustomLink>
          </Form>
        </Navbar.Collapse>
      </nav>
    </Navbar>
  );
}

function CustomLink({ to, children, ...props }) {
  const resolvedPath = useResolvedPath(to);
  const isActive = useMatch({ path: resolvedPath.pathname, end: true });

  return (
    <li className={isActive ? "active" : ""}>
      <Link to={to} {...props}>
        {children}
      </Link>
    </li>
  );
}
