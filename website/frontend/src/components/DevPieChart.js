import { PieChart, Pie, Cell, Tooltip, Legend } from "recharts";
import condition_severity from "../visualization/condition_severity.json";

function DevPieChart() {

    const data = Object.entries(condition_severity).map(([name, value]) => ({
        name: String(name),
        value: Number(value)
    }));

    const pieChartColors = [
        '#AA80AA', // Blue-Violet
        '#80AAAA', // Yellow-Green
        '#AA44AA', // Red-Orange
    ];




    return (
        <PieChart width={600} height={600}>
            <Pie
                data={data}
                dataKey="value"
                nameKey="name"
                cx="50%"
                cy="50%"
                outerRadius={200}
                labelLine={false}
                fill="#8884d8"
            >
                {data.map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={pieChartColors[index % pieChartColors.length]} />
                ))}
            </Pie>
            <Tooltip />
            <Legend />
        </PieChart>
    )
}

export default DevPieChart;