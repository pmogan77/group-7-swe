import React from 'react';
import { Container } from 'react-bootstrap';
import '../css-styles/DescriptionBox.css'; // Import your CSS file

const JobDescription = ({ contents }) => {
    return (
        <Container className="job-text" style={{ maxHeight: '500px', overflowY: 'auto' }}>
            <h1>Job Description:</h1>
            <div dangerouslySetInnerHTML={{ __html: contents }} />
        </Container>
    );
};

export default JobDescription;
