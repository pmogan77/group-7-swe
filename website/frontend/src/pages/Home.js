import React from "react";
import "../css-styles/Home.css";
import HomeCard from '../components/HomeCard';
import jobimage from "../media/job-test.png";
import houseimage from "../media/house-test.jpg";
import resourceimage from "../media/immigrant-test.jpg";

export default function Home() {
  return (
    <>
      <div className="home-container">
        <div className="bg-image">

          <div className="text-overlay">
            <h1 id="large-font">A Warm Welcome to LA</h1>

            {/* add some more text about immigratnts in LA */}

            <p>
              Learn more about immigrant resources, affordable housing, and jobs in
              Los Angeles.

              If you are interested in donating, please visit our about page.
            </p>

          </div>
        </div>
      </div>
      <div className="container text-center">
        <div className="row">
          <div className="col">
            <HomeCard title="Jobs" text="Find jobs that fit your skills" link="/Jobs" image={jobimage} />
          </div>
          <div className="col">
            <HomeCard title="Housing" text="Discover affordable housing" link="/AffordableHousing" image={houseimage} />
          </div>
          <div className="col">
            <HomeCard title="Resources" text="Explore resources in the LA region" link="/ImmigrationResources" image={resourceimage} />
          </div>
        </div>
      </div>

    </>
  );
}
