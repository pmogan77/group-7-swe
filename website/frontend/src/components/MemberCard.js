import React from "react";
import Card from "react-bootstrap/Card";
import "../css-styles/MemberCard.css";

function MemberCard({
  name,
  bio,
  responsibilities,
  gitID,
  commits,
  issues,
  tests,
  image,
}) {
  return (
    <Card style={{ width: "18rem" }} id="member-card-full">
      <Card.Img variant="top" src={image} />
      <Card.Body id="card-body">
        <Card.Title id="member-name">{name[0]}</Card.Title>
        <hr />
        <Card.Text>{"Role: " + responsibilities}</Card.Text>
        <Card.Text>{"Bio: " + bio} </Card.Text>
        <Card.Text> {"GitLab ID: " + gitID}</Card.Text>
        <Card.Text> {"Commits: " + commits}</Card.Text>
        <Card.Text>{"Issues: " + issues} </Card.Text>
        <Card.Text> {"Tests: " + tests}</Card.Text>
      </Card.Body>
    </Card>
  );
}

export default MemberCard;
