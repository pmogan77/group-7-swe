import axios from "axios";

export const getJobs = async ({
  currentPage = 1,
  jobQuery = '',
  jobLocation = '',
  jobCommunity = '',
  jobSortOrder = '',
  categoryType = ''
}) => {
  try {

    if (jobSortOrder === "A-Z") jobSortOrder = "asc";
    if (jobSortOrder === "Z-A") jobSortOrder = "desc";

    const response = await axios.get('https://api.la-integration-initiative.me/job', {
      params: {
        page: currentPage,
        per_page: 20,
        query: jobQuery,
        locations: jobLocation,
        community: jobCommunity,
        sort_order: jobSortOrder,
        category_type: categoryType
      },
    });
    return response.data;
  } catch (error) {
    console.error('Error:', error);

  }
}

export const getHouses = async ({
  currentPage = 1,
  houseQuery = '',
  activeFinancing = '',
  maxRent = '',
  houseSortOrder = ''


}) => {
  try {
    if (houseSortOrder === "A-Z") houseSortOrder = "asc";
    if (houseSortOrder === "Z-A") houseSortOrder = "desc";

    if (activeFinancing === "Active Financing") activeFinancing = "Y";
    if (activeFinancing === "Inactive Financing") activeFinancing = "N";


    const response = await axios.get('https://api.la-integration-initiative.me/house', {
      params: {
        page: currentPage,
        per_page: 20,
        query: houseQuery,
        active_financing: activeFinancing,
        max_rent: maxRent,
        sort_order: houseSortOrder,
      },
    });
    return response.data;
  } catch (error) {
    console.error('Error:', error);

  }
}

export const getResources = async ({
  currentPage = 1,
  resourceQuery = '',
  resourceZip = '',
  resourceCommunity = '',
  resourceSortOrder = '',
}) => {
  try {
    if (resourceSortOrder === "A-Z") resourceSortOrder = "asc";
    if (resourceSortOrder === "Z-A") resourceSortOrder = "desc";

    const response = await axios.get('https://api.la-integration-initiative.me/immigration', {
      params: {
        page: currentPage,
        per_page: 20,
        query: resourceQuery,
        zip: resourceZip,
        community: resourceCommunity,
        sort_order: resourceSortOrder,
      },
    });
    return response.data;
  } catch (error) {
    console.error('Error:', error);

  }
}

export const getSearch = async ({
  search_text = '',
  models = '',
}) => {
  try {
    const response = await axios.get('https://api.la-integration-initiative.me/search', {
      params: {
        query: search_text,
        models: models
      },
    });
    // console.log(JSON.stringify(response.data));
    return response.data;
  } catch (error) {
    console.error('Error:', error);

  }
}


