import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import JobCard from "../components/JobCard";
import "../css-styles/ModelPage.css";
import CustomDropdown from "../components/Dropdown";
import Pagination from "../components/Pagination";
import { getJobs } from "../api"
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function Jobs() {

  const [search, setSearch] = useState('');
  const [location, setLocation] = useState("");
  const [categoryType, setCategorType] = useState("");
  const [sortType, setSortType] = useState("");
  const [itemsPerPage, setItemsPerPage] = useState(20);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(10);
  const [totalInstances, setTotalInstances] = useState(20);
  const [jobs, setJobs] = useState([]);
  const allCategories = [
    "Business Operations",
    "Food and Hospitality Services",
    "Software Engineering",
    "Legal Services",
    "Retail",
    "",
    "Human Resources and Recruitment",
    "Education",
    "Construction",
    "Sales",
    "Healthcare",
    "Advertising and Marketing",
    "Installation, Maintenance, and Repairs",
    "Accounting and Finance",
    "Project Management",
    "Entertainment and Travel Services",
    "Science and Engineering",
    "Unknown",
    "Management",
    "Data and Analytics",
    "Manufacturing and Warehouse",
    "Animal Care",
    "Protective Services",
    "Product Management",
    "Media, PR, and Communications",
    "Sports, Fitness, and Recreation",
    "Transportation and Logistics",
    "Design and UX",
    "Administration and Office",
    "Writing and Editing",
    "Computer and IT",
    "Arts",
    "Account Management",
    "Customer Service",
    "Personal Care and Services",
    "Cleaning and Facilities",
    "Social Services"
  ];

  const allLocations = [
    "San Dimas, CA",
    "Palmdale, CA",
    "Torrance, CA",
    "Duarte, CA",
    "West Hollywood, CA",
    "Los Angeles, CA",
    "Aliso Viejo, CA",
    "Long Beach, CA",
    "Paramount, CA",
    "Glendale, CA",
    "Huntington Beach, CA",
    "Cerritos, CA",
    "La Mirada, CA",
    "Burbank, CA",
    "Pasadena, CA",
    "Anaheim, CA",
    "Compton, CA",
    "Culver City, CA",
    "Redondo Beach, CA",
    "Manhattan Beach, CA",
    "Santa Monica, CA"
  ]

  const handleSearch = (e) => {
    e.preventDefault();
    setSearch(search);
  };

  // Search Bar Data
  const tokens = search.split(' ');
  const search_text = tokens.join(' ');

  useEffect(() => {

    //make call to api to get jobs
    const fetchData = async () => {
      const result = await getJobs({ currentPage: currentPage, jobSortOrder: sortType, categoryType: categoryType, jobLocation: location, jobQuery: search_text });
      setJobs(result.results);
      setTotalInstances(result.total_jobs);
      setItemsPerPage(result.results.length);
      setTotalPages(Math.ceil(totalInstances / 20));



    };
    fetchData();




  }, [location, categoryType, sortType, currentPage, totalInstances, itemsPerPage, search, search_text]);



  return (
    <Container className="full-page">

      <div className="searchContainer">
        <Form className="d-flex" onSubmit={handleSearch}>
          <Form.Control
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <Button variant="outline-primary" onClick={handleSearch}>Search</Button>
        </Form>
      </div>
      <div className="dropdownContainer">
        <CustomDropdown title="Location" items={allLocations} setterFunc={setLocation}></CustomDropdown>
        <CustomDropdown title="Category Type" items={allCategories} setterFunc={setCategorType}></CustomDropdown>
        <CustomDropdown title="Sort By Name" items={["A-Z", "Z-A"]} setterFunc={setSortType}></CustomDropdown>
      </div>

      <div className="pagination-component-container">
        <Pagination totalPages={totalPages} setPage={setCurrentPage}></Pagination>
      </div>

      <Container id="page-container">


        {jobs.map((job, index) => {
          return <JobCard key={index} jobsData={jobs} num={index} highlight={search_text} />;
        })}




      </Container>
      <footer>Displaying {itemsPerPage} results of {totalInstances} </footer>
    </Container>
  );
}


export default Jobs;