import React, { useEffect } from 'react';
import mapboxgl from 'mapbox-gl';


//call by sending center as array like
// <Map center = {[long,lat]} />
const Map = ({center}) => {
  
  useEffect(() => {
    const [lon, lat] = center; 

    mapboxgl.accessToken = 'pk.eyJ1IjoieW9wcm8iLCJhIjoiY2xuMHNtZzk0MXJhZjJqdDM5NGp0aXFkOCJ9.FcxjpWITg8PGwqy_wEvdJg';
    const map = new mapboxgl.Map({
      container: 'map-container',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lon, lat],
      zoom: 12 // Default zoom level
      
    });
    new mapboxgl.Marker()
      .setLngLat([lon, lat])
      .addTo(map);

    // Clean up the map when the component unmounts
    return () => map.remove();
  }, [center]); // Empty dependency array ensures useEffect runs once

  return <div id="map-container" style={{ width: '50%', height: '50vh', margin: 'auto' }} />;
};

export default Map;
