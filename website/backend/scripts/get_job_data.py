import requests
import json

# Initialize an empty list to store job data
all_jobs = []

# Define the base URL and query parameters
base_url = "https://www.themuse.com/api/public/jobs"
query_params = {
    "location": [
        "Acton, CA", "Agoura Hills, CA", "Aliso Viejo, CA", "Anaheim, CA", "Beverly Hills, CA", "Burbank, CA", 
        "Cerritos, CA", "Compton, CA", "Culver City, CA", "Duarte, CA", "El Segundo, CA", "Glendale, CA", 
        "Huntington Beach, CA", "La Mirada, CA", "Long Beach, CA", "Los Angeles, CA", "Malibu, CA", 
        "Manhattan Beach, CA", "Palmdale, CA", "Paramount, CA", "Pasadena, CA", "Redondo Beach, CA", 
        "San Dimas, CA", "Santa Clara, CA", "Santa Monica, CA", "Torrance, CA", "West Hollywood, CA", 
        "Westlake Village, CA"
    ],
}

# Set the initial page number and page count
page_number = 1
page_count = 787

# Iterate through all pages
while page_number <= page_count:
    # Add the current page number to the query parameters
    query_params["page"] = page_number

    # Make the API request
    response = requests.get(base_url, params=query_params)

    # Check if the request was successful
    if response.status_code == 200:
        # Parse the JSON response
        data = json.loads(response.text)

        # Filter jobs with "Flexible / Remote" location and add them to the list
        filtered_jobs = [job for job in data["results"] if all(location["name"] != "Flexible / Remote" for location in job["locations"])]
        all_jobs.extend(filtered_jobs)

        # Increment the page number
        page_number += 1
    else:
        print(f"Error fetching data for page {page_number}: {response.status_code}")
        print(response)
        print(base_url)
        break

# Save the collected job data to a JSON file
with open("filtered_jobs.json", "w") as json_file:
    json.dump(all_jobs, json_file)

print(f"Total {len(all_jobs)} jobs saved to 'filtered_jobs.json'")
