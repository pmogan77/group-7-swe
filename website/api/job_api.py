import requests
import json

api_url = "https://www.themuse.com/api/public/jobs?page="

page_number = 1

while True:
    new_api_url = api_url + str(page_number)
    response_API = requests.get(new_api_url)
    if (response_API.status_code != 200):
        break
    json_object = json.loads(response_API.text)
    data = json.dumps(json_object, indent=2)
    print(data)
    page_number += 1

print("Finished Scraping Job Data")