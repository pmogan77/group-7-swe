import requests
import json

api_url = "http://3.144.76.176:8080/get_drugs"

# Fetch data from the API
response = requests.get(api_url)
data = response.json()

# Create a dictionary to store side effect frequency count
side_effect_count = {}

# Process the data and count the frequency of each side effect
for entry in data:
    side_effects = entry.get("sideEffects", "").split(",")  # Assuming side effects are comma-separated
    for side_effect in side_effects:
        side_effect = side_effect.strip()
        side_effect_count[side_effect] = side_effect_count.get(side_effect, 0) + 1

# Convert the dictionary to the desired format
formatted_data = [{"subject": f"{i+1} {side_effect}", "A": count} for i, (side_effect, count) in enumerate(side_effect_count.items())]

# Create a JSON file with the formatted data
output_file_path = "formatted_side_effect_frequency.json"
with open(output_file_path, "w") as json_file:
    json.dump(formatted_data, json_file, indent=2)

print(f"Formatted side effect frequency data has been saved to {output_file_path}")