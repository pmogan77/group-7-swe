import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import HouseCard from "../components/HouseCard";

import "../css-styles/ModelPage.css";
import CustomDropdown from "../components/Dropdown";
import Pagination from "../components/Pagination";
import { getHouses } from "../api";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

function AffordableHousing() {

  const [search, setSearch] = useState('');
  const [maxRent, setMaxRent] = useState("");
  const [activeFinanacing, setActiveFinancing] = useState("");
  const [sortType, setSortType] = useState("");
  const [itemsPerPage, setItemsPerPage] = useState(20);
  const [totalPages, setTotalPages] = useState(10);
  const [totalInstances, setTotalInstances] = useState(20);
  const [currentPage, setCurrentPage] = useState(1);
  const [houses, setHouses] = useState([]);

  const handleSearch = (e) => {
    e.preventDefault();
    setSearch(search);
  };

  const tokens = search.split(' ');
  const search_text = tokens.join(' ');

  useEffect(() => {

    //make call to api to get houses
    const fetchData = async () => {
      const result = await getHouses({ currentPage: currentPage, houseSortOrder: sortType, activeFinancing: activeFinanacing, maxRent: maxRent, houseQuery: search_text });
      setHouses(result.results);
      setTotalInstances(result.total_housing);

      setItemsPerPage(result.results.length);
      setTotalPages(Math.ceil(totalInstances / 20));
      console.log(result)

    };

    fetchData();


  }, [maxRent, activeFinanacing, sortType, itemsPerPage, currentPage, totalInstances, search, search_text]);




  return (
    <Container className="full-page">
      <div className="searchContainer">
        <Form className="d-flex" onSubmit={handleSearch}>
          <Form.Control
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <Button variant="outline-primary" onClick={handleSearch}>Search</Button>
        </Form>
      </div>
      <div className="dropdownContainer">
        <CustomDropdown title="Max Rent $" items={[100, 200, 300, 400, 500, 600, 700, 800, 900]} setterFunc={setMaxRent}></CustomDropdown>
        <CustomDropdown title="Filter by Financing" items={["Active Financing", "Inactive Financing"]} setterFunc={setActiveFinancing}></CustomDropdown>
        <CustomDropdown title="Sort By Name" items={["A-Z", "Z-A"]} setterFunc={setSortType}></CustomDropdown>
      </div>

      <div className="pagination-component-container">
        <Pagination totalPages={totalPages} setPage={setCurrentPage}></Pagination>
      </div>

      <Container id="page-container">
        {houses.map((house, index) => {
          return <HouseCard key={index} homeData={houses} num={index} highlight={search_text} />;
        })}

      </Container>
      <footer>Displaying {itemsPerPage} out of {totalInstances} results</footer>
    </Container>
  );
}


export default AffordableHousing;