import requests
import json

api_url_base = 'https://api.la-integration-initiative.me/house'

# Function to fetch data from the API
def fetch_data(page, per_page=1000):
    try:
        response = requests.get(f'{api_url_base}?page={page}&per_page={per_page}')
        data = response.json()
        return data
    except Exception as e:
        print(f'Error fetching data for page {page}: {e}')
        return None

# Function to process the data and group by rent per month
def group_by_rent(data):
    if not data or not data.get('results'):
        print('Invalid data format or empty results.')
        return None

    rent_groups = {}

    # Process each result
    for item in data['results']:
        rent = item.get('rent_per_month', 0)

        # Determine the rent group
        rent_group = (rent // 100) * 100

        # Initialize or increment the count for the rent group
        rent_groups[rent_group] = rent_groups.get(rent_group, 0) + 1
        

    return dict(sorted(rent_groups.items()))

# Function to write the result to a JSON file
def write_to_json_file(data, filename='rent_groups.json'):
    with open(filename, 'w') as file:
        json.dump(data, file, indent=2)
    print(f'Results written to {filename}')

# Main execution
def main():
    # Make an initial call to get information about total objects and items per page
    initial_data = fetch_data(1)
    total_objects = initial_data.get('total_housing', 0)
    per_page = initial_data.get('per_page', 2)

    if total_objects == 0:
        print('Failed to retrieve total objects information. Exiting.')
        return

    rent_groups = group_by_rent(initial_data)

    # Write the final result to a JSON file
    write_to_json_file(rent_groups)

# Run the script
if __name__ == '__main__':
    main()
