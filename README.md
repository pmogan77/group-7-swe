# LA Integration Initiative, CS 373 SWE, Fall 2023

# Canvas / Ed Discussion Group Number

Group 7

# Website link
https://la-integration-initiative.me/

# Names of the team members
 - Josue Martinez
 - Sean Dudo
 - Praveen Mogan
 - Jacob Villanueva

# Name of the Project:
LA-Integration-Initiative

# The Proposed Project:
Our project serves immigrants within Los Angeles. We will provide additional resources to help address job security, affordable housing, and immigration resources.This will help immigrants coming into LA by providing them the resources they need to integrate into their new lives seamlessly.

Los Angeles has 4.4 million immigrants, representing 33% of the population of the city. In addition, this group has been expanding over time. In order to help this group of people adapt to their lives within the LA region, we are providing resources that can help them integrate into the community.

# URLs of at least three data sources that you will programmatically scrape using a RESTful API: 
 - https://www.themuse.com/developers/api/v2 (this dataset has a filter for jobs within the LA region that accept immigrant workers)
 - https://hub.arcgis.com/datasets/HUD::multifamily-properties-assisted/api (this dataset has a filter for homes in the LA region that are more afforable, which may be affordable for those just moving into the area)
 - https://geohub.lacity.org/datasets/fe51a86cdf184bfca9302d1b7433b1c2/api

# At least 3 models
 - Jobs in Los Angeles
 - Affordable Housing in Los Angeles
 - Immigration Resources in Los Angeles

# An estimate of the number of instances of each model
 - Jobs in Los Angeles(200)
 - Affordable Housing in Los Angeles (250)
 - Immigration Resources in Los Angeles (90)

# Each model must have many attributes
 - Jobs in Los Angeles
    - description
    - name
    - location
    - publish date
    - company
    - category
    - etc.
 - Affordable Housing in Los Angeles
    - is under management
    - has active financing
    - people per unit
    - total available units
    - rent per month
    - etc.
 - Immigration Resources in Los Angeles
    - description
    - name of resource
    - business hours
    - location
    - link to resource
    - etc.
    
# Describe five of those attributes for each model that you can filter or sort
 - Jobs
    - company name
    - category type
    - name of job
    - publish date
    - location
 - Affordable Housing
    - is under management
    - has active financing
    - people per unit
    - total available units
    - rent per month
 - Immigration Resources in Los Angeles
    - description
    - name of resource
    - business hours
    - Sub-city
    - Zip Code
    - etc.

# Describe two types of media for instances of each model
 - Jobs
    - Map that displays job location within Los Angeles
    - Text data regarding the job
 - Affordable Housing
    - Map that displays location
    - On site property phone number
    - Various text information regarding the property itself
 - Immigration Resources in Los Angeles
    - Map that displays location of resource
    - Text information about the resource

# Describe three questions that your site will answer
 - Where can I find jobs based on my skill level?
 - Where can I find afforable housing?
 - Where can I find resources that aid immigrants ?

# Total Estimated Time: 
 - Phase 1: 70 Hours
 - Phase 2: 90 Hours
 - Phase 3: 40 Hours

# Total Actual Time: 
 - Phase 1: 80 Hours
 - Phase 2: 100 Hours
 - Phase 3: 30 Hours

# Member Estimated Time Splits:
- Phase 1: 
  - Josue Martinez: 15 Hours
  - Sean Dudo: 15 Hours
  - Songsen Chen: 10 Hours
  - Praveen Mogan: 15 Hours
  - Jacob Villanueva: 15 Hours

 - Phase 2 :
   - Josue Martinez: 20 Hours
   - Sean Dudo: 20 Hours
   - Songsen Chen: 10 Hours
   - Praveen Mogan: 20 Hours
   - Jacob Villanueva: 20 Hours

 - Phase 3 :
   - Josue Martinez: 10 Hours
   - Sean Dudo: 10 Hours
   - Praveen Mogan: 10 Hours
   - Jacob Villanueva: 10 Hours

# Member Actual Time Splits:
- Phase 1: 
  - Josue Martinez: 20 Hours
  - Sean Dudo: 20 Hours
  - Songsen Chen: 5 Hours
  - Praveen Mogan: 20 Hours
  - Jacob Villanueva: 15 Hours

 - Phase 2 :
   - Josue Martinez: 25 Hours
   - Sean Dudo: 25 Hours
   - Songsen Chen: 5 Hours
   - Praveen Mogan: 25 Hours
   - Jacob Villanueva: 20 Hours

 - Phase 3 :
   - Josue Martinez: 7 Hours
   - Sean Dudo: 8 Hours
   - Praveen Mogan: 8 Hours
   - Jacob Villanueva: 7 Hours

# Phase Leader:
 - Phase 1: Josue Martinez
 - Phase 2: Praveen Mogan
 - Phase 3: Sean Dudo

# Responsibilities of Phase Leader
 - Host daily group stand-ups
 - Lead development meetings with both teams
   - Backend
   - Frontend
 - Assign tasks to members
 - Serve as Project Manager
 - Main point of outreach to TAs 

# Latest SHA:
 - 51d6646f7449cd962e3095f1379e25521b33ec99

# Backend API Link
 - https://api.la-integration-initiative.me/

# API Documentation Link
 - https://documenter.getpostman.com/view/14095754/2s9YJXYQNV

# Database configuration (PlantUML)
![Database Configuration](./PlantUML.png)

# Database configuration (DBDiagram)
![Database Configuration](./DBDiagram.png)