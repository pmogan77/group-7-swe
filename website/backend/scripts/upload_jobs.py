import mysql.connector
import json
import os

from dotenv import load_dotenv
load_dotenv()

def connect_to_database():
    connection = mysql.connector.connect(
        host=os.environ.get("sql_host"),
        user=os.environ.get("sql_username"),
        password=os.environ.get("sql_password"),
        database=os.environ.get("sql_database")
    )
    return connection


def insert_immigration_data(data):
    connection = connect_to_database()
    cursor = connection.cursor()

    for result in data["results"]:
        # Extract data from JSON
        job_id = result["id"]  # Extract the ID field
        name = result["name"]
        contents = result["contents"]
        publication_date = result["publication_date"]
        locations = [location["name"] for location in result["locations"]]  # Extract location names into a list
        categories = [category["name"] for category in result["categories"]]  # Extract category names into a list
        company_name = result["company"]["name"]
        refs = result["refs"]["landing_page"]

        # Convert lists to comma-separated strings
        locations_str = ';'.join(locations)
        categories_str = ';'.join(categories)

        # Define the INSERT query
        insert_query = """
            INSERT INTO jobs (id, name, categories, publication_date, locations, company, refs, contents)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
        """

        # Execute the INSERT query
        cursor.execute(insert_query, (job_id, name, categories_str, publication_date, locations_str, company_name, refs, contents))

    # Commit the changes and close the connection
    connection.commit()
    cursor.close()
    connection.close()


def read_json_data():
    from pathlib import Path

    path = Path(__file__).parent
    with open(path/"filtered_jobs.json", 'r', encoding="utf8") as json_file:
        data = json.load(json_file)
    return data

if __name__ == "__main__":
    data_to_insert = read_json_data()
    insert_immigration_data(data_to_insert)