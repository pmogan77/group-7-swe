import aws from "./media/aws-logo.jpg";
import gitlab from "./media/gitlab-logo.jpg";
import namecheap from "./media/namecheap-logo.jpg";
import react from "./media/react-logo.jpg";
import postman from "./media/postman-logo.jpg";

const tools = [
  {
    name: "Amazon Web Service",
    image: aws,
  },

  {
    name: "GitLab",
    image: gitlab,
  },

  {
    name: "Namecheap",
    image: namecheap,
  },

  {
    name: "React",
    image: react,
  },
  {
    name: "Postman",
    image: postman,
  },
];

export { tools };
