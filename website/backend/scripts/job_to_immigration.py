import json
import random
import mysql.connector
from math import radians, cos, sin, asin, sqrt

from pathlib import Path

path = Path(__file__).parent

import os

from dotenv import load_dotenv
load_dotenv()

# Load the jobs dataset, immigration services dataset, and city lat/lon mapping from JSON files
with open(path/"filtered_jobs.json", 'r') as jobs_file:
    jobs_data = json.load(jobs_file)['results']

with open(path/"immigration.json", 'r') as immigration_file:
    immigration_data = json.load(immigration_file)['features']

with open(path/"city.json", 'r') as city_file:
    city_data = json.load(city_file)


# Function to calculate the distance between two points using Haversine formula
def calculate_distance(lat1, lon1, lat2, lon2):
    # Radius of the Earth in miles
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2

    c = 2 * asin(sqrt(a))

    # Radius of the earth in kilometers. Use 3956 for miles
    r = 6371

    # calculate the result
    return c * r * 0.621371

# Function to perform case-insensitive substring search
def case_insensitive_substring(city, text):
    return city.lower() in text.lower()

# Function to get immigration services near a job within a specified distance
def get_immigration_services_near_job(job_lat, job_lon, immigration_data, radius):
    nearby_immigration_services = []
    for service in immigration_data:
        service_lat = service['geometry']['y']
        service_lon = service['geometry']['x']

        # Calculate the distance between the job and the immigration service
        distance = calculate_distance(job_lat, job_lon, service_lat, service_lon)

        if distance <= radius:  # Check if the immigration service is within the specified distance
            nearby_immigration_services.append(service['attributes']['OBJECTID'])  # Append service ID to the list

    return nearby_immigration_services

conn = mysql.connector.connect(
    host=os.environ.get("sql_host"),
        user=os.environ.get("sql_username"),
        password=os.environ.get("sql_password"),
        database=os.environ.get("sql_database")
)
cursor = conn.cursor()

# Iterate through each job in the dataset
for job in jobs_data:
    job_id = job['id']

    job_name = job['name']
    job_lat = 34.0522
    job_lon = -118.2437

    # Check if any city substring is a case-insensitive match in the job's name
    for city_info in city_data:
        if case_insensitive_substring(city_info['city'], job_name):
            job_lat = city_info['lat']
            job_lon = city_info['long']
            break

    # Find immigration services within a specified distance of the job
    nearby_immigration_services = get_immigration_services_near_job(job_lat, job_lon, immigration_data, 20)

    if len(nearby_immigration_services) < 3:
        nearby_immigration_services = get_immigration_services_near_job(job_lat, job_lon, immigration_data, 49)

    if len(nearby_immigration_services) > 5:
        nearby_immigration_services = random.sample(nearby_immigration_services, 5)

    if len(nearby_immigration_services) > 0:
        # Insert job ID and immigration service IDs into the MySQL table
        for service_id in nearby_immigration_services:
            cursor.execute('INSERT INTO job_to_immigration (job_id, immigration_id) VALUES (%s, %s)', (job_id, service_id))

    # Commit changes to the MySQL database
    conn.commit()

    print(f"Job ID: {job_id}")
    for service_id in nearby_immigration_services:
        print("\tImmigration Service ID:", service_id)

# Close the MySQL database connection
conn.close()