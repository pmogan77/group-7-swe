import React from "react";
import Container from "react-bootstrap/Container";
import "../css-styles/ModelPage.css";
import DevPieChart from "../components/DevPieChart";
import DevBarGraph from "../components/DevBarGraph";
import DevRadarChart from "../components/DevRadarChart";


function DevViz() {

    return (
        <Container className="full-page">
            <h1 className="title">Developer's Visualizations</h1>
            <Container className="charts-container">
                <h2>Frequency of Condition's Severity</h2>
                <Container className="severity-container">
                    <DevPieChart />
                </Container>
                <h2>Frequency of Effect's Severity</h2>
                <Container className="effect-container">
                    <DevBarGraph />
                </Container>
                <h2>Frequency of Side Effects of Drugs</h2>
                <Container className="side-effect-container">
                    <DevRadarChart />
                </Container>
            </Container>
        </Container>
    );
}


export default DevViz;