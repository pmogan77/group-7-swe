import { PieChart, Pie, Cell, Tooltip, Legend } from "recharts";
import rent_data from "../visualization/rent_groups.json";

function RentPieChart() {

    const data = Object.entries(rent_data).map(([name, value]) => ({
        name: String(name),
        value: Number(value)
    }));

    const pieChartColors = [
        '#4CAF50', // Green
        '#2196F3', // Blue
        '#FFC107', // Yellow
        '#FF5722', // Orange
        '#9C27B0', // Purple
        '#00BCD4', // Cyan
        '#FF9800', // Amber
        '#795548', // Brown
        '#607D8B', // Blue Grey
        '#E91E63', // Pink
    ];




    return (
        <PieChart width={600} height={600}>
            <Pie
                data={data}
                dataKey="value"
                nameKey="name"
                cx="50%"
                cy="50%"
                outerRadius={200}
                labelLine={false}
                fill="#8884d8"
            >
                {data.map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={pieChartColors[index % pieChartColors.length]} />
                ))}
            </Pie>
            <Tooltip />
            <Legend />
        </PieChart>
    )
}

export default RentPieChart;