import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import "../css-styles/Search.css"
import React, { useState, useEffect } from "react";
import { getSearch } from "../api";
import JobCard from "../components/JobCard";
import ResourceCard from "../components/ResourceCard";
import HouseCard from "../components/HouseCard";
import Container from "react-bootstrap/Container";

import catholic from "../media/catholic.png";

export default function Search({ searchQuery }) {
    const [jobs, setJobs] = useState([]);
    const [houses, setHouses] = useState([]);
    const [resources, setResources] = useState([]);

    const tokens = searchQuery.split(' ');
    const search_text = tokens.join(' ');

    useEffect(() => {

        // make call to api to get search
        const fetchData = async () => {
            const result = await getSearch({ search_text })
            setJobs(result.job);
            setHouses(result.housing);
            setResources(result.immigration);
        };
        fetchData();

    }, [search_text]);

    return (
        <Tabs
            defaultActiveKey="jobs"
            id="fill-tab-example"
            className="search-tabs"
            justify
        >
            <Tab eventKey="jobs" title="Jobs">
                <Container id="page-container">
                    {jobs.map((job, index) => {
                        return <JobCard key={index} jobsData={jobs} num={index} highlight={search_text} />;
                    })}
                </Container>
            </Tab>
            <Tab eventKey="housing" title="Affordable Housing">
                <Container id="page-container">
                    {houses.map((house, index) => {
                        return <HouseCard key={index} homeData={houses} num={index} highlight={search_text} />;
                    })}

                </Container>
            </Tab>
            <Tab eventKey="resources" title="Immigration Resources">
                <Container id="page-container">
                    {resources.map((resource, index) => {
                        return <ResourceCard key={index} image={catholic} resourceData={resources} num={index} highlight={search_text} />;
                    })}
                </Container>
            </Tab>
        </Tabs>
    );
}