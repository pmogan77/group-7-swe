import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import zip_data from "../visualization/zip_code_counts.json";


function ResourceBarGraph() {

    const data = Object.entries(zip_data).map(([zip, count]) => ({
        zip: String(zip),
        count: Number(count)
    }));

    return (
        <BarChart width={800} height={400} data={data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="zip" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar type="monotone" dataKey="count" stroke="#8884d8" fill="#8051A0" />
        </BarChart>
    );
}


export default ResourceBarGraph;
