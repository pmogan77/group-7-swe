import requests
import json

response_API = requests.get('https://services.arcgis.com/VTyQ9soqVukalItT/arcgis/rest/services/MULTIFAMILY_PROPERTIES_ASSISTED/FeatureServer/7/query?where=1%3D1&outFields=*&outSR=4326&f=json')

print("API status code: ", response_API.status_code)
json_object = json.loads(response_API.text)
data = json.dumps(json_object, indent=2)
print(data)