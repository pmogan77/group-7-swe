import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import job_data from "../visualization/job_frequency.json";


function JobLineGraph() {

    const data = Object.entries(job_data).map(([date, jobs]) => ({
        date: String(date),
        jobs: Number(jobs)
    }));

    return (
        <LineChart width={800} height={400} data={data}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="date" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="jobs" stroke="#8884d8" strokeWidth={3} />
        </LineChart>
    );
}


export default JobLineGraph;
