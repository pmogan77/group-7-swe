// import Card from "../components/Card";
// import "../css-styles/Card.css";
// import Container from "react-bootstrap/esm/Container";
// import immigrationImage from "../media/immigrant-test.jpg"; // Import the image for this specific page
import catholic from "../media/catholic.png";


import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import ResourceCard from "../components/ResourceCard";
import CustomDropdown from "../components/Dropdown";
import "../css-styles/ModelPage.css";
import Pagination from "../components/Pagination";
import { getResources } from "../api";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function ImmigrationResources() {

  const [search, setSearch] = useState('');
  const [city, setCity] = useState("");
  const [zipCode, setZipCode] = useState("");
  const [sortType, setSortType] = useState("");
  const [itemsPerPage, setItemsPerPage] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalInstances, setTotalInstances] = useState(20);
  const [totalPages, setTotalPages] = useState(10);
  const [resources, setResources] = useState([]);
  const allZipCodes = [
    "90010",
    "90304",
    "90015",
    "90745",
    "91204",
    "90005",
    "90017",
    "91502",
    "91343",
    "90007",
    "91331",
    "93550",
    "90744",
    "90023",
    "91731",
    "91205",
    "90019",
    "90022",
    "90044",
    "90057",
    "90249",
    "90706",
    "90240",
    "90201",
    "91340",
    "90014",
    "90039",
    "90650",
    "91335",
    "90018",
    "91352",
    "90066",
    "90032",
    "90028",
    "90045",
    "90046",
    "90280",
    "90026",
    "91411",
    "90037",
    "90035",
    "91766",
    "91733",
    "90029",
    "91104",
    "91202",
    "90012",
    "90004",
    "91746",
    "90025",
    "90250",
    "90505",
    "90277",
    "91367",
    "90036",
    "90504",
    "90501",
    "90301",
    "90020"
  ];

  const allCommunities = [
    "Los Angeles",
    "Lennox",
    "Carson",
    "Glendale",
    "Burbank",
    "North Hills",
    "Pacoima",
    "Palmdale",
    "Wilmington",
    "El Monte",
    "Gardena",
    "Bellflower",
    "Downey",
    "Bell",
    "San Fernando",
    "Norwalk",
    "Reseda",
    "Sun Valley",
    "South Gate",
    "Van Nuys",
    "Pomona",
    "San Bernardino",
    "South El Monte",
    "Hollywood",
    "Pasadena",
    "Bell Gardens",
    "La Puente",
    "Hawthorne",
    "Torrance",
    "Redondo Beach",
    "Woodland Hills",
    "Inglewood"
  ];


  const handleSearch = (e) => {
    e.preventDefault();
    setSearch(search);
  };

  // Search Bar Data
  const tokens = search.split(' ');
  const search_text = tokens.join(' ');


  useEffect(() => {

    //make call to api to get jobs
    const fetchData = async () => {
      const result = await getResources({ currentPage: currentPage, resourceSortOrder: sortType, resourceZip: zipCode, resourceCommunity: city, resourceQuery: search_text });
      setResources(result.results);
      setTotalInstances(result.total_immigrations);
      setItemsPerPage(result.results.length);
      setTotalPages(Math.ceil(totalInstances / 20));
    };

    fetchData();

  }, [zipCode, city, sortType, itemsPerPage, currentPage, totalInstances, search, search_text]);


  return (
    <Container className="full-page">

      <div className="searchContainer">
        <Form className="d-flex" onSubmit={handleSearch}>
          <Form.Control
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <Button variant="outline-primary" onClick={handleSearch}>Search</Button>
        </Form>
      </div>

      <div className="dropdownContainer">
        <CustomDropdown title="Community" items={allCommunities} setterFunc={setCity}></CustomDropdown>
        <CustomDropdown title="Zip Code" items={allZipCodes} setterFunc={setZipCode}></CustomDropdown>
        <CustomDropdown title="Sort By Name" items={["A-Z", "Z-A"]} setterFunc={setSortType}></CustomDropdown>
      </div>

      <div className="pagination-component-container">
        <Pagination totalPages={totalPages} setPage={setCurrentPage}></Pagination>
      </div>

      <Container id="page-container">
        {resources.map((resource, index) => {
          return <ResourceCard key={index} image={catholic} resourceData={resources} num={index} highlight={search_text} />;
        })}


      </Container>
      <footer>Displaying {itemsPerPage} out of {totalInstances} results</footer>
    </Container>
  );
}