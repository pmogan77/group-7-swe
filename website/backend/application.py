from flask import Flask, request, jsonify
from flask_cors import CORS
from dotenv import load_dotenv
from sqlalchemy import or_

load_dotenv()

import os

# Create a Flask web application
application = Flask(__name__)
CORS(application)

from flask_sqlalchemy import SQLAlchemy

# Configure the database URI
application.config["SQLALCHEMY_DATABASE_URI"] = (
    "mysql://"
    + os.environ.get("sql_usernameRO")
    + ":"
    + os.environ.get("sql_passwordRO")
    + "@"
    + os.environ.get("sql_host")
    + ":3306/"
    + os.environ.get("sql_database")
)
db = SQLAlchemy(application)


# Import your models here
class Job(db.Model):
    __tablename__ = "jobs"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    categories = db.Column(db.String(500))
    publication_date = db.Column(db.String(100))
    locations = db.Column(db.String(500))
    company = db.Column(db.String(100))
    refs = db.Column(db.String(500))
    contents = db.Column(db.String)  # Adjust the data type as needed

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "categories": self.categories,
            "publication_date": self.publication_date,
            "locations": self.locations,
            "company": self.company,
            "refs": self.refs,
            "contents": self.contents,
        }


class Immigration(db.Model):
    __tablename__ = "immigration"

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(4000))
    name = db.Column(db.String(150))
    hours = db.Column(db.String(1000))
    addrln1 = db.Column(db.String(75))
    url = db.Column(db.String(150))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    community = db.Column(db.String(150))
    zip = db.Column(db.Integer)

    def serialize(self):
        return {
            "id": self.id,
            "description": self.description,
            "name": self.name,
            "hours": self.hours,
            "addrln1": self.addrln1,
            "url": self.url,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "community": self.community,
            "zip": self.zip,
        }


class Housing(db.Model):
    __tablename__ = "housing"

    id = db.Column(db.Integer, primary_key=True)
    property_name_text = db.Column(db.String(50), nullable=False)
    is_under_management_ind = db.Column(db.CHAR(1))
    has_active_financing_ind = db.Column(db.CHAR(1))
    people_per_unit = db.Column(db.Float, nullable=False)
    total_avbl_units = db.Column(db.Integer, nullable=False)
    rent_per_month = db.Column(db.Integer, nullable=False)
    mgmt_agent_org_name = db.Column(db.String(100))
    mgmt_contact_full_name = db.Column(db.String(56))
    mgmt_contact_address_line1 = db.Column(db.String(45))
    mgmt_contact_city_name = db.Column(db.String(28))
    mgmt_contact_state_code = db.Column(db.CHAR(2))
    mgmt_contact_zip_code = db.Column(db.CHAR(5))
    mgmt_contact_main_phn_nbr = db.Column(db.String(25))
    mgmt_contact_email_text = db.Column(db.String(100))
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)

    def serialize(self):
        return {
            "id": self.id,
            "property_name_text": self.property_name_text,
            "is_under_management_ind": self.is_under_management_ind,
            "has_active_financing_ind": self.has_active_financing_ind,
            "people_per_unit": self.people_per_unit,
            "total_avbl_units": self.total_avbl_units,
            "rent_per_month": self.rent_per_month,
            "mgmt_agent_org_name": self.mgmt_agent_org_name,
            "mgmt_contact_full_name": self.mgmt_contact_full_name,
            "mgmt_contact_address_line1": self.mgmt_contact_address_line1,
            "mgmt_contact_city_name": self.mgmt_contact_city_name,
            "mgmt_contact_state_code": self.mgmt_contact_state_code,
            "mgmt_contact_zip_code": self.mgmt_contact_zip_code,
            "mgmt_contact_main_phn_nbr": self.mgmt_contact_main_phn_nbr,
            "mgmt_contact_email_text": self.mgmt_contact_email_text,
            "lat": self.lat,
            "lon": self.lon,
        }


class HouseToJob(db.Model):
    __tablename__ = "house_to_job"

    house_id = db.Column(db.Integer, db.ForeignKey("housing.id"), primary_key=True)
    job_id = db.Column(db.Integer, db.ForeignKey("jobs.id"), primary_key=True)

    def serialize(self):
        return {"house_id": self.house_id, "job_id": self.job_id}


class HouseToImmigration(db.Model):
    __tablename__ = "house_to_immigration"

    house_id = db.Column(db.Integer, db.ForeignKey("housing.id"), primary_key=True)
    immigration_id = db.Column(
        db.Integer, db.ForeignKey("immigration.id"), primary_key=True
    )

    def serialize(self):
        return {"house_id": self.house_id, "immigration_id": self.immigration_id}


class JobToImmigration(db.Model):
    __tablename__ = "job_to_immigration"

    job_id = db.Column(db.Integer, db.ForeignKey("jobs.id"), primary_key=True)
    immigration_id = db.Column(
        db.Integer, db.ForeignKey("immigration.id"), primary_key=True
    )

    def serialize(self):
        return {"job_id": self.job_id, "immigration_id": self.immigration_id}


class JobToHouse(db.Model):
    __tablename__ = "job_to_house"

    job_id = db.Column(db.Integer, db.ForeignKey("jobs.id"), primary_key=True)
    house_id = db.Column(db.Integer, db.ForeignKey("housing.id"), primary_key=True)

    def serialize(self):
        return {"job_id": self.job_id, "house_id": self.house_id}


class ImmigrationToHouse(db.Model):
    __tablename__ = "immigration_to_house"

    immigration_id = db.Column(
        db.Integer, db.ForeignKey("immigration.id"), primary_key=True
    )
    house_id = db.Column(db.Integer, db.ForeignKey("housing.id"), primary_key=True)

    def serialize(self):
        return {"immigration_id": self.immigration_id, "house_id": self.house_id}


class ImmigrationToJob(db.Model):
    __tablename__ = "immigration_to_job"

    immigration_id = db.Column(
        db.Integer, db.ForeignKey("immigration.id"), primary_key=True
    )
    job_id = db.Column(db.Integer, db.ForeignKey("jobs.id"), primary_key=True)

    def serialize(self):
        return {"immigration_id": self.immigration_id, "job_id": self.job_id}


# Create the API manager
@application.route("/job", methods=["GET"])
def get_jobs():
    # Define default values for query parameters
    page = request.args.get("page", default=1, type=int)
    per_page = request.args.get("per_page", default=20, type=int)
    query = request.args.get("query", default="", type=str)
    category_type = request.args.get("category_type", default="", type=str)
    locations = request.args.get("locations", default="", type=str)
    sort_order = request.args.get("sort_order", default="asc", type=str)

    # Query the database based on the provided query parameters
    jobs_query = Job.query

    query_terms = []  # Initialize the query_terms list here

    if query:
        # Search for the query string in multiple fields
        query_terms = query.split()

        # Initialize an empty list to store individual ilike filters
        ilike_filters = []

        # Create ilike filters for each field and each query term
        for term in query_terms:
            ilike_filters.extend(
                [
                    Job.name.ilike(f"%{term}%"),
                    Job.categories.ilike(f"%{term}%"),
                    Job.locations.ilike(f"%{term}%"),
                    Job.company.ilike(f"%{term}%"),
                    Job.refs.ilike(f"%{term}%"),
                    Job.contents.ilike(f"%{term}%"),
                ]
            )

        # Combine the ilike filters with an or_ to match any word in the query
        query_filter = or_(*ilike_filters)
        jobs_query = jobs_query.filter(query_filter)

    if category_type:
        jobs_query = jobs_query.filter(Job.categories.ilike(f"%{category_type}%"))

    if locations:
        jobs_query = jobs_query.filter(Job.locations.ilike(f"%{locations}%"))

    # Retrieve the results without sorting for now
    job_results = jobs_query.all()

    # Calculate relevance scores for each result
    relevance_scores = [
        (result, calculate_relevance(query_terms, result.serialize()))
        for result in job_results
    ]

    # Sort results by relevance score and name
    if sort_order == "asc":
        if query:
            sorted_jobs = sorted(relevance_scores, key=lambda x: x[1])
        else:
            sorted_jobs = sorted(relevance_scores, key=lambda x: x[0].name)
    else:
        if query:
            sorted_jobs = sorted(relevance_scores, key=lambda x: -x[1])
        else:
            sorted_jobs = sorted(
                relevance_scores, key=lambda x: x[0].name, reverse=True
            )

    # Paginate the sorted results
    total_jobs = len(sorted_jobs)
    start_idx = (page - 1) * per_page
    end_idx = start_idx + per_page
    paginated_jobs = sorted_jobs[start_idx:end_idx]

    # Extract the sorted job items
    sorted_jobs = [result for result, relevance in paginated_jobs]

    # Serialize the paginated jobs
    serialized_jobs = [job.serialize() for job in sorted_jobs]

    return jsonify(
        {
            "results": serialized_jobs,
            "page": page,
            "per_page": per_page,
            "total_jobs": total_jobs,
        }
    )


@application.route("/immigration", methods=["GET"])
def get_immigrations():
    page = request.args.get("page", default=1, type=int)
    per_page = request.args.get("per_page", default=20, type=int)
    query = request.args.get("query", default="", type=str)
    zip_code = request.args.get("zip", default=None, type=str)
    community = request.args.get("community", default="", type=str)
    sort_order = request.args.get("sort_order", default="asc", type=str)

    # Query the database based on the provided query parameters
    immigration_query = Immigration.query
    query_terms = []

    if query:
        # Search for the query string in multiple fields
        query_terms = query.split()

        # Initialize an empty list to store individual ilike filters
        ilike_filters = []

        # Create ilike filters for each field and each query term
        for term in query_terms:
            ilike_filters.extend(
                [
                    Immigration.description.ilike(f"%{term}%"),
                    Immigration.name.ilike(f"%{term}%"),
                    Immigration.hours.ilike(f"%{term}%"),
                    Immigration.addrln1.ilike(f"%{term}%"),
                ]
            )

        # Combine the ilike filters with an or_ to match any word in the query
        query_filter = or_(*ilike_filters)
        immigration_query = immigration_query.filter(query_filter)

    if zip_code:
        immigration_query = immigration_query.filter(Immigration.zip == zip_code)

    if community:
        immigration_query = immigration_query.filter(
            Immigration.community.ilike(f"%{community}%")
        )

    # Retrieve the results without sorting for now
    immigration_results = immigration_query.all()

    # Calculate relevance scores for each result
    relevance_scores = [
        (result, calculate_relevance(query_terms, result.serialize()))
        for result in immigration_results
    ]

    # Sort results by relevance score and name
    if sort_order == "asc":
        if query:
            sorted_immigrations = sorted(relevance_scores, key=lambda x: x[1])
        else:
            sorted_immigrations = sorted(relevance_scores, key=lambda x: x[0].name)
    else:
        if query:
            sorted_immigrations = sorted(relevance_scores, key=lambda x: -x[1])
        else:
            sorted_immigrations = sorted(
                relevance_scores, key=lambda x: x[0].name, reverse=True
            )

    # Paginate the sorted results
    total_immigrations = len(sorted_immigrations)
    start_idx = (page - 1) * per_page
    end_idx = start_idx + per_page
    paginated_immigrations = sorted_immigrations[start_idx:end_idx]

    # Extract the sorted immigration items
    sorted_immigrations = [result for result, relevance in paginated_immigrations]

    # Serialize the paginated immigrations
    serialized_immigrations = [
        immigration.serialize() for immigration in sorted_immigrations
    ]

    return jsonify(
        {
            "results": serialized_immigrations,
            "page": page,
            "per_page": per_page,
            "total_immigrations": total_immigrations,
        }
    )


@application.route("/house", methods=["GET"])
def get_housing():
    page = request.args.get("page", default=1, type=int)
    per_page = request.args.get("per_page", default=20, type=int)
    query = request.args.get("query", default="", type=str)
    active_financing = request.args.get("active_financing", default=None, type=str)
    max_rent = request.args.get("max_rent", default=None, type=float)
    sort_order = request.args.get("sort_order", default="asc", type=str)

    # Query the database based on the provided query parameters
    housing_query = Housing.query

    query_terms = []

    if query:
        # Search for the query string in multiple fields
        query_terms = query.split()

        # Initialize an empty list to store individual ilike filters
        ilike_filters = []

        # Create ilike filters for each field and each query term
        for term in query_terms:
            ilike_filters.extend(
                [
                    Housing.property_name_text.ilike(f"%{term}%"),
                    Housing.mgmt_agent_org_name.ilike(f"%{term}%"),
                    Housing.mgmt_contact_full_name.ilike(f"%{term}%"),
                    Housing.mgmt_contact_address_line1.ilike(f"%{term}%"),
                    Housing.mgmt_contact_city_name.ilike(f"%{term}%"),
                    Housing.mgmt_contact_email_text.ilike(f"%{term}%"),
                ]
            )

        # Combine the ilike filters with an or_ to match any word in the query
        query_filter = or_(*ilike_filters)
        housing_query = housing_query.filter(query_filter)

    if active_financing:
        housing_query = housing_query.filter(
            Housing.has_active_financing_ind == active_financing
        )

    if max_rent is not None:
        housing_query = housing_query.filter(Housing.rent_per_month <= max_rent)

    # Retrieve the results without sorting for now
    housing_results = housing_query.all()

    # Calculate relevance scores for each result
    relevance_scores = [
        (result, calculate_relevance(query_terms, result.serialize()))
        for result in housing_results
    ]

    # Sort results by relevance score and property name
    if sort_order == "asc":
        if query:
            sorted_results = sorted(relevance_scores, key=lambda x: x[1])
        else:
            sorted_results = sorted(
                relevance_scores, key=lambda x: x[0].property_name_text
            )
    else:
        if query:
            sorted_results = sorted(relevance_scores, key=lambda x: -x[1])
        else:
            sorted_results = sorted(
                relevance_scores, key=lambda x: x[0].property_name_text, reverse=True
            )

    # Extract the sorted housing items
    sorted_housing = [result for result, _ in sorted_results]

    # Paginate the sorted results
    total_housing = len(sorted_housing)
    start_idx = (page - 1) * per_page
    end_idx = start_idx + per_page
    paginated_housing = sorted_housing[start_idx:end_idx]

    # Serialize the paginated housing
    serialized_housing = [house.serialize() for house in paginated_housing]

    return jsonify(
        {
            "results": serialized_housing,
            "page": page,
            "per_page": per_page,
            "total_housing": total_housing,
        }
    )


@application.route("/job/<int:job_id>", methods=["GET"])
def get_job_by_id(job_id):
    job = Job.query.get(job_id)
    if job:
        return jsonify(job.serialize())
    else:
        return jsonify({"message": "Job not found"}), 404


@application.route("/immigration/<int:immigration_id>", methods=["GET"])
def get_immigration_by_id(immigration_id):
    immigration = Immigration.query.get(immigration_id)
    if immigration:
        return jsonify(immigration.serialize())
    else:
        return jsonify({"message": "Immigration not found"}), 404


@application.route("/house/<int:house_id>", methods=["GET"])
def get_house_by_id(house_id):
    house = Housing.query.get(house_id)
    if house:
        return jsonify(house.serialize())
    else:
        return jsonify({"message": "House not found"}), 404


@application.route("/search", methods=["GET"])
def google_like_search():
    # Get the query string from the request
    query = request.args.get("query", default="", type=str)

    # Tokenize the input query into individual terms
    query_terms = query.split()

    # Define a list of model classes to search
    model_classes = [Job, Immigration, Housing]  # Add other models as needed

    # Initialize a dictionary to store search results by model
    search_results = {}

    # Set a limit on the maximum number of results
    max_results_per_model = 100

    # Search across all models
    for model_class in model_classes:
        # Initialize a list to store search results for the current model
        model_results = []

        # Search for the full input phrase first
        i = 0
        for result in model_class.query.all():
            # Convert the model object to a dictionary for serialization
            result_dict = result.serialize()

            # Calculate the relevance score for the result
            relevance_score = calculate_relevance(query_terms, result_dict)

            if relevance_score > 0:
                # Add the result and its relevance score to the list
                model_results.append((result_dict, relevance_score))
                i += 1

            # Limit the number of results to the specified maximum
            if i >= max_results_per_model:
                break

        # If no results are found for the full phrase, search for individual terms
        if not model_results:
            for result in model_class.query.all():
                result_dict = result.serialize()
                relevance_score = calculate_relevance(query_terms, result_dict)

                if relevance_score > 0:
                    model_results.append((result_dict, relevance_score))

                # Limit the number of results to the specified maximum
                if len(model_results) >= max_results_per_model:
                    break

        # Sort the results by relevance score in descending order
        model_results.sort(key=lambda x: x[1], reverse=True)

        # Serialize and store the top results for the current model, respecting the limit
        top_results = [result[0] for result in model_results[:max_results_per_model]]
        search_results[model_class.__name__.lower()] = top_results

    return jsonify(search_results)


def calculate_relevance(query_terms, result_dict):
    # Calculate the relevance score based on the number of matched terms
    relevance_score = 0

    # Convert all model attribute values to a single string for searching
    result_text = "".join(str(value) for value in result_dict.values()).lower()

    # check if the full query term is in the result text
    if " ".join(query_terms).lower() in result_text:
        relevance_score += 1000

    for term in query_terms:
        if term in result_text:
            # Increase relevance score for each matched term
            relevance_score += 1

    return relevance_score


@application.route("/job/<int:job_id>/nearbyHouses", methods=["GET"])
def get_houses_near_job(job_id):
    try:
        # Query the JobToHouse table to get house IDs related to the specified job_id
        house_ids_near_job = (
            JobToHouse.query.filter(JobToHouse.job_id == job_id)
            .with_entities(JobToHouse.house_id)
            .all()
        )

        if not house_ids_near_job:
            return jsonify({"error": "No houses found near the job"}), 404

        # Extract house IDs from the query result
        house_ids = [house_id[0] for house_id in house_ids_near_job]

        # Query the Housing table to get house information for the house IDs
        houses_near_job = Housing.query.filter(Housing.id.in_(house_ids)).all()

        # Serialize the house information
        serialized_houses = [house.serialize() for house in houses_near_job]

        return jsonify({"houses": serialized_houses}), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500


@application.route("/job/<int:job_id>/nearbyImmigration", methods=["GET"])
def get_immigration_resources_near_job(job_id):
    try:
        # Query the JobToImmigration table to get immigration resource IDs related to the specified job_id
        immigration_resource_ids_near_job = (
            JobToImmigration.query.filter(JobToImmigration.job_id == job_id)
            .with_entities(JobToImmigration.immigration_id)
            .all()
        )

        if not immigration_resource_ids_near_job:
            return (
                jsonify({"error": "No immigration resources found near the job"}),
                404,
            )

        # Extract immigration resource IDs from the query result
        immigration_resource_ids = [
            immigration_id[0] for immigration_id in immigration_resource_ids_near_job
        ]

        # Query the Immigration table to get immigration resource information for the resource IDs
        immigration_resources_near_job = Immigration.query.filter(
            Immigration.id.in_(immigration_resource_ids)
        ).all()

        # Serialize the immigration resource information
        serialized_immigration_resources = [
            resource.serialize() for resource in immigration_resources_near_job
        ]

        return jsonify({"immigrations": serialized_immigration_resources}), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500


@application.route("/house/<int:house_id>/nearbyJobs", methods=["GET"])
def get_nearby_jobs_for_house(house_id):
    try:
        # Query the HouseToJob table to get job IDs related to the specified house_id
        job_ids_near_house = (
            HouseToJob.query.filter(HouseToJob.house_id == house_id)
            .with_entities(HouseToJob.job_id)
            .all()
        )

        if not job_ids_near_house:
            return jsonify({"error": "No jobs found near the house"}), 404

        # Extract job IDs from the query result
        job_ids = [job_id[0] for job_id in job_ids_near_house]

        # Query the Job table to get job information for the job IDs
        jobs_near_house = Job.query.filter(Job.id.in_(job_ids)).all()

        # Serialize the job information
        serialized_jobs = [job.serialize() for job in jobs_near_house]

        return jsonify({"jobs": serialized_jobs}), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500


@application.route("/house/<int:house_id>/nearbyImmigration", methods=["GET"])
def get_nearby_immigration_for_house(house_id):
    try:
        # Query the HouseToImmigration table to get immigration IDs related to the specified house_id
        immigration_ids_near_house = (
            HouseToImmigration.query.filter(HouseToImmigration.house_id == house_id)
            .with_entities(HouseToImmigration.immigration_id)
            .all()
        )

        if not immigration_ids_near_house:
            return (
                jsonify({"error": "No immigration resources found near the house"}),
                404,
            )

        # Extract immigration IDs from the query result
        immigration_ids = [
            immigration_id[0] for immigration_id in immigration_ids_near_house
        ]

        # Query the Immigration table to get immigration resource information for the immigration IDs
        immigration_resources_near_house = Immigration.query.filter(
            Immigration.id.in_(immigration_ids)
        ).all()

        # Serialize the immigration resource information
        serialized_immigration_resources = [
            resource.serialize() for resource in immigration_resources_near_house
        ]

        return jsonify({"immigrations": serialized_immigration_resources}), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500


@application.route("/immigration/<int:immigration_id>/nearbyJobs", methods=["GET"])
def get_nearby_jobs_for_immigration(immigration_id):
    try:
        # Query the ImmigrationToJob table to get job IDs related to the specified immigration_id
        job_ids_near_immigration = (
            ImmigrationToJob.query.filter(
                ImmigrationToJob.immigration_id == immigration_id
            )
            .with_entities(ImmigrationToJob.job_id)
            .all()
        )

        if not job_ids_near_immigration:
            return (
                jsonify({"error": "No jobs found near the immigration resource"}),
                404,
            )

        # Extract job IDs from the query result
        job_ids = [job_id[0] for job_id in job_ids_near_immigration]

        # Query the Job table to get job information for the job IDs
        jobs_near_immigration = Job.query.filter(Job.id.in_(job_ids)).all()

        # Serialize the job information
        serialized_jobs_near_immigration = [
            job.serialize() for job in jobs_near_immigration
        ]

        return jsonify({"jobs": serialized_jobs_near_immigration}), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500


@application.route("/immigration/<int:immigration_id>/nearbyHouses", methods=["GET"])
def get_nearby_housing_for_immigration(immigration_id):
    try:
        # Query the ImmigrationToHouse table to get housing IDs related to the specified immigration_id
        housing_ids_near_immigration = (
            ImmigrationToHouse.query.filter(
                ImmigrationToHouse.immigration_id == immigration_id
            )
            .with_entities(ImmigrationToHouse.house_id)
            .all()
        )

        if not housing_ids_near_immigration:
            return (
                jsonify({"error": "No housing found near the immigration resource"}),
                404,
            )

        # Extract housing IDs from the query result
        housing_ids = [housing_id[0] for housing_id in housing_ids_near_immigration]

        # Query the Housing table to get housing information for the housing IDs
        housing_near_immigration = Housing.query.filter(
            Housing.id.in_(housing_ids)
        ).all()

        # Serialize the housing information
        serialized_housing_near_immigration = [
            house.serialize() for house in housing_near_immigration
        ]

        return jsonify({"houses": serialized_housing_near_immigration}), 200

    except Exception as e:
        return jsonify({"error": str(e)}), 500


@application.route("/", methods=["GET"])
def default_route():
    return "Success", 200


if __name__ == "__main__":
    application.run(debug=True, host="0.0.0.0")
