import Josue from "./media/josueheadshot.jpg";
import Praveen from "./media/praveenheadshot.jpg";
import Jacob from "./media/jacobheadshot.jpg";
import Sean from "./media/seanheadshot.jpg";

const members = [
  {
    name: ["Josue Martinez", "Josue Arturo Martinez"],
    bio: "Junior at UT Austin studying CS and also enjoys Taekwondo.",
    responsibilities: "Frontend",
    gitID: "josueamm",
    commits: 0,
    issues: 0,
    tests: 0,
    image: Josue,
  },



  {
    name: ["Praveen Mogan", ""],
    bio: "Junior at UT Austin studying Computer Science. In my free time, I play  basketball and racketball recreationally",
    responsibilities: "Backend",
    gitID: "pmogan77",
    commits: 0,
    issues: 0,
    tests: 0,
    image: Praveen,
  },

  {
    name: ["Jacob Villanueva", ""],
    bio: "Junior at The University of Texas at Austin studying Computer Science and Mathematics. Interests in Derivatives Trading and Global Macro.",
    responsibilities: "Backend",
    gitID: "jacobvillanueva",
    commits: 0,
    issues: 0,
    tests: 0,
    image: Jacob,
  },

  {
    name: ["Sean Dudo", "Sean"],
    bio: "Senior at UT Austin studying computer science. Interested in game development and software engineering.",
    responsibilities: "Frontend",
    gitID: "Pseudo-Sudo-Dudo",
    commits: 0,
    issues: 0,
    tests: 0,
    image: Sean,
  },
];

export { members };
