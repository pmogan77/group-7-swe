import React from "react";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import "../css-styles/ToolCard.css";

function CharityCard({ name, image, link, bio=""}) {
  return (
    <Link to={link} target="_blank" rel="noopener noreferrer">
      <Card style={{ width: "18rem" }} id="tool-box">
        <Card.Img variant="top" src={image} />
        <Card.Body id="card-body">
          <Card.Title className="tools-card-title"> {name}</Card.Title>
          <Card.Text> {bio} </Card.Text>
        </Card.Body>
      </Card>
    </Link>
  );
}

export default CharityCard;
