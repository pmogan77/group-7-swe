import unittest
from application import application as app


class TestEndpoints(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_get_jobs_length(self):
        response = self.app.get("/job")
        self.assertEqual(response.status_code, 200)
        data = response.json.get("results")
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 20)

    def test_get_immigrations_length(self):
        response = self.app.get("/immigration")
        self.assertEqual(response.status_code, 200)
        data = response.json.get("results")
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 20)

    def test_get_housing_length(self):
        response = self.app.get("/house")
        self.assertEqual(response.status_code, 200)
        data = response.json.get("results")
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 20)

    def test_get_job_by_id(self):
        response = self.app.get("/job/12283633")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)

    def test_get_immigration_by_id(self):
        response = self.app.get("/immigration/603")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)

    def test_get_house_by_id(self):
        response = self.app.get("/house/800002875")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)

    def test_search_across_models(self):
        response = self.app.get("/search?query=query")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(response.json.get("job")), 10)

    def test_get_houses_near_job(self):
        response = self.app.get("/job/12283633/nearbyHouses")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(response.json.get("houses")), 5)

    def test_get_immigration_resources_near_job(self):
        response = self.app.get("/job/12283633/nearbyImmigration")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(response.json.get("immigrations")), 5)

    def test_get_nearby_jobs_for_house(self):
        response = self.app.get("/house/800002875/nearbyJobs")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(response.json.get("jobs")), 5)

    def test_get_nearby_immigration_for_house(self):
        response = self.app.get("/house/800002875/nearbyImmigration")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(response.json.get("immigrations")), 5)

    def test_get_nearby_jobs_for_immigration(self):
        response = self.app.get("/immigration/603/nearbyJobs")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(response.json.get("jobs")), 5)

    def test_get_nearby_housing_for_immigration(self):
        response = self.app.get("/immigration/603/nearbyHouses")
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIsNotNone(data)
        self.assertEqual(len(response.json.get("houses")), 5)

    def test_default_route(self):
        response = self.app.get("/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Success")

    def test_get_jobs_pagination(self):
        response = self.app.get("/job?page=1&per_page=10")
        self.assertEqual(response.status_code, 200)
        data = response.json.get("results")
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 10)

    def test_get_immigrations_pagination(self):
        response = self.app.get("/immigration?page=1&per_page=10")
        self.assertEqual(response.status_code, 200)
        data = response.json.get("results")
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 10)

    def test_get_housing_pagination(self):
        response = self.app.get("/house?page=1&per_page=10")
        self.assertEqual(response.status_code, 200)
        data = response.json.get("results")
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 10)


if __name__ == "__main__":
    unittest.main()
