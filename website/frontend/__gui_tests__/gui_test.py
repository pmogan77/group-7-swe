import unittest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://la-integration-initiative.me/"

# URL = "http://localhost:3000/"

class Test(unittest.TestCase):

    # class method set up & tear down from https://gitlab.com/visiopi/re-park-able/-/blob/main/frontend/my-app/__gui_tests__/gui_Tests.py?ref_type=heads
    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--window-size=1920x1080")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}

        self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
        self.driver.maximize_window()
        self.driver.get(URL)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_home_URL(self):
        extension = ""
        self.driver.get(URL + extension)
        self.assertEqual(self.driver.current_url, URL + extension)

    def test_about_URL(self):
        extension = "About"
        self.driver.get(URL + extension)
        self.assertEqual(self.driver.current_url, URL + extension)
    
    def test_job_URL(self):
        extension = "Jobs"
        self.driver.get(URL + extension)
        self.assertEqual(self.driver.current_url, URL + extension)

    def test_housing_URL(self):
        extension = "AffordableHousing"
        self.driver.get(URL + extension)
        self.assertEqual(self.driver.current_url, URL + extension)

    def test_resources_URL(self):
        extension = "ImmigrationResources"
        self.driver.get(URL + extension)
        self.assertEqual(self.driver.current_url, URL + extension)

    def test_about_Navbar(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'About')))
        link = self.driver.find_element(By.LINK_TEXT, "About")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "About"))

        self.assertEqual(self.driver.current_url, URL + "About")

    def test_jobs_Navbar(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'About')))
        link = self.driver.find_element(By.LINK_TEXT, "Jobs")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "Jobs"))

        self.assertEqual(self.driver.current_url, URL + "Jobs")

    def test_housing_Navbar(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'About')))
        link = self.driver.find_element(By.LINK_TEXT, "Affordable Housing")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "AffordableHousing"))

        self.assertEqual(self.driver.current_url, URL + "AffordableHousing")

    def test_resources_Navbar(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'About')))
        link = self.driver.find_element(By.LINK_TEXT, "Immigration Resources")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "ImmigrationResources"))

        self.assertEqual(self.driver.current_url, URL + "ImmigrationResources")

    def test_round_robin_Navbar(self):
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, 'About')))
        # Goes to about page
        link = self.driver.find_element(By.LINK_TEXT, "About")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "About"))

        self.assertEqual(self.driver.current_url, URL + "About")

        # Goes to job page
        link = self.driver.find_element(By.LINK_TEXT, "Jobs")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "Jobs"))

        self.assertEqual(self.driver.current_url, URL + "Jobs")

        # Goes to affordable housing page
        link = self.driver.find_element(By.LINK_TEXT, "Affordable Housing")
        
        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "AffordableHousing"))

        self.assertEqual(self.driver.current_url, URL + "AffordableHousing")

        # Goes to immigration resources page
        link = self.driver.find_element(By.LINK_TEXT, "Immigration Resources")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "ImmigrationResources"))

        self.assertEqual(self.driver.current_url, URL + "ImmigrationResources")

        # Goes back home
        link = self.driver.find_element(By.LINK_TEXT, "Home")

        link.click()

        WebDriverWait(self.driver, 10).until(EC.url_matches(URL + "Home"))

        self.assertEqual(self.driver.current_url, URL + "Home")

if __name__ == "__main__":
    unittest.main()
