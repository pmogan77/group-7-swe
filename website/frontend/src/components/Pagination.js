import { useEffect, useState } from 'react';
import "../css-styles/Pagination.css"

function CustomButton({activePage,startNum,index,handlePageClick}) {
    return (
        <li className="page-item"  key={index}>
            <button className="page-link" id={`${activePage === index + startNum ? 'page-button-active' : 'page-button'}`} onClick={() => handlePageClick(index + startNum)}>{index + startNum}</button>
        </li>
    )
}

function Pagination({totalPages, setPage}) {
    const [screenSize] = useState(window.innerWidth);
    const [startNum, setStartNum] = useState(1);
    const [activePage, setActivePage] = useState(1);

    const buttonList = [];
    for (let i = 0; i < Math.min(5, totalPages); i++) {
        buttonList.push(
            <CustomButton index={i} activePage={activePage} startNum={startNum} handlePageClick={handlePageClick} />
        )
    }

    function handlePageClick(pageNum) {

        if (activePage === pageNum) return;

        setActivePage(pageNum);

        if (totalPages < 5) return;

        let newStartNum = pageNum - 2;
        if (newStartNum < 1) {
            newStartNum = 1;
        } else if (pageNum + 2 > totalPages) {
            newStartNum = totalPages - 4;
        }
        setStartNum(newStartNum);
    }

    function handlePrevClick() {
        if (startNum > 1) {
            setStartNum(startNum - 1);
            setActivePage(activePage - 1);
        } else if (activePage > 1) {
            setActivePage(activePage - 1);
        }
    }

    function handleNextClick() {
        if (startNum < totalPages - 4) {
            setStartNum(startNum + 1);
            setActivePage(activePage + 1);
        } else if (activePage < totalPages) {
            setActivePage(activePage + 1);
        }
    }

    useEffect(() => {
        setPage(activePage);
    }, [activePage, setPage]);

    return (
        <>
            <nav>
                <ul className={`pagination pagination-${screenSize > 800 ? 'lg' : 'md'}`}>
                    <li className="page-item" onClick={handlePrevClick}>
                        <button className="page-link" id={`${activePage === 1 ? 'page-button-disabled' : 'page-button'}`}> {"<"} </button>
                    </li>
                    {buttonList}
                    <li className="page-item" onClick={handleNextClick}>
                        <button className="page-link" id={`${activePage === totalPages ? 'page-button-disabled' : 'page-button'}`} > {">"} </button>
                    </li>
                </ul>
            </nav>
        </>
    );
}

export default Pagination;
