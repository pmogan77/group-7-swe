import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Link } from 'react-router-dom';
import { Highlight } from 'react-highlight-regex'
import "../css-styles/Card.css";

function JobCard({ jobsData, num, highlight = null }) {
  const jobData = num !== undefined ? jobsData[num] : jobsData;

  if (!jobData) {
    return <div>Job not found</div>;
  }

  const { name, categories, publication_date, locations, company, id } = jobData;

  // Format the date to look pretty
  const formattedDate = new Date(publication_date).toLocaleDateString();

  // maps all the locations and categories
  const locationNames = Array.isArray(locations) ? locations.map(location => location.name).join(', ') : locations;
  const categoryNames = Array.isArray(categories) ? categories.map(category => category.name).join(', ') : categories;

  const jobId = id;
  const jobInstancesURL = `/JobInstance/${jobId}`;

  const highlightText = (input) => {
    if (highlight) {
      const highlightRegex = new RegExp(highlight, "i");
      return <Highlight highlightClassname={"mark"} match={highlightRegex} text={input} />;
    }
    return input;
  };

  return (
    <Card style={{ width: "16rem" }} id="full-card">
      <Card.Body id="card-body">
        <Container className="card-info">
          <Card.Title id="title">{highlightText(name)}</Card.Title>
          <Card.Text>{highlightText(`Company: ${company}`)}</Card.Text>
          <Card.Text>{highlightText(`Category: ${categoryNames}`)} </Card.Text>
          <Card.Text> {highlightText(`Publish Date: ${formattedDate}`)}</Card.Text>
          <Card.Text> {highlightText(`Location: ${locationNames}`)}</Card.Text>
          <div className="button-container">
            <Link to={jobInstancesURL}>
              <Button variant="light" className="custom-button">Learn More</Button>
            </Link>
          </div>
        </Container>
      </Card.Body>
    </Card>
  );
}

export default JobCard;
