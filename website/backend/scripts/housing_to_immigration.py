import json
import mysql.connector
from math import radians, cos, sin, asin, sqrt
import random

from pathlib import Path

path = Path(__file__).parent

from dotenv import load_dotenv
load_dotenv()

import os

# Load the housing dataset, immigration services dataset, and city lat/lon mapping from JSON files
with open(path / "filtered_housing.json", "r") as housing_file:
    housing_data = json.load(housing_file)

with open(path/"immigration.json", 'r') as immigration_file:
    immigration_data = json.load(immigration_file)['features']

# Function to calculate the distance between two points using Haversine formula
def calculate_distance(lat1, lon1, lat2, lon2):
    # Radius of the Earth in miles
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    lat1 = radians(lat1)
    lat2 = radians(lat2)
      
    # Haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
 
    c = 2 * asin(sqrt(a)) 
    
    # Radius of earth in kilometers. Use 3956 for miles
    r = 6371
      
    # calculate the result
    return(c * r * 0.621371)

# Function to get immigration services near a housing location within a specified distance
def get_immigration_services_near_house(house_lat, house_lon, immigration_data, radius):
    nearby_immigration_services = []
    for service in immigration_data:
        service_lat = service['geometry']['y']
        service_lon = service['geometry']['x']

        # Calculate the distance between the house and the service
        distance = calculate_distance(house_lat, house_lon, service_lat, service_lon)

        if distance <= radius:  # Check if the service is within the specified distance
            nearby_immigration_services.append(service['attributes']['OBJECTID'])  # Append service ID to the list

    return nearby_immigration_services

conn = mysql.connector.connect(
    host=os.environ.get("sql_host"),
    user=os.environ.get("sql_username"),
    password=os.environ.get("sql_password"),
    database=os.environ.get("sql_database")
)
cursor = conn.cursor()

# Iterate through each house in the housing dataset
for house in housing_data['results']:
    house_lat = house['LAT']
    house_lon = house['LON']
    house_id = house['PROPERTY_ID']

    # Find immigration services within a specified distance of the house
    nearby_immigration_services = get_immigration_services_near_house(house_lat, house_lon, immigration_data, 20)

    if len(nearby_immigration_services) < 3:
        nearby_immigration_services = get_immigration_services_near_house(house_lat, house_lon, immigration_data, 36)

    if len(nearby_immigration_services) > 5:
        nearby_immigration_services = random.sample(nearby_immigration_services, 5)

    for service_id in nearby_immigration_services:
            cursor.execute('INSERT INTO house_to_immigration (house_id, immigration_id) VALUES (%s, %s)', (house_id, service_id))
    conn.commit()


    print(f"House ID: {house_id}")
    for service_id in nearby_immigration_services:
        print("\tImmigration Service ID:", service_id)

# Close the MySQL database connection
conn.close()
