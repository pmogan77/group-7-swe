import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Link } from 'react-router-dom';
import { Highlight } from 'react-highlight-regex'
import "../css-styles/Card.css";

function ResourceCard({ resourceData, num, highlight = null }) {
  const resources = num !== undefined ? resourceData[num] : resourceData;

  if (!resources) {
    return <div>Resource not found</div>;
  }

  const {
    description,
    name,
    hours,
    addrln1,
    url,
    id
  } = resources;

  const urlWithProtocol = "https://" + url;

  const resourceId = id;
  const resourceInstancesURL = `/ResourcesInstance/${resourceId}`;

  const highlightText = (input) => {
    if (highlight) {
      const highlightRegex = new RegExp(highlight, "i");
      return <Highlight highlightClassname={"mark"} match={highlightRegex} text={input} />;
    }
    return input;
  };

  const linkStyle = {
    color: 'orange', // Change this to the desired color
  };

  return (
    <Card style={{ width: "16rem" }} id="full-card">
      <Card.Body id="card-body">
        <Container className="card-info">
          <Card.Title id="title">{highlightText(name)}</Card.Title>
          <Card.Text>{highlightText(description)}</Card.Text>
          <Card.Text>{highlightText(`Hours: ${hours !== null ? hours : "N/A"}`)} </Card.Text>
          <Card.Text> {highlightText(`Location: ${addrln1}`)}</Card.Text>
          <Card.Text>
            Website URL: <a href={urlWithProtocol} target="_blank" rel="noopener noreferrer" style={linkStyle}>{url}</a>
          </Card.Text>
          <div className="button-container">
            <Link to={resourceInstancesURL}>
              <Button variant="light">Learn More</Button>
            </Link>
          </div>
        </Container>
      </Card.Body>
    </Card>
  );
}

export default ResourceCard;
